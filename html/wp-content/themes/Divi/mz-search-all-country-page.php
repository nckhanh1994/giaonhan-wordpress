<?php
    /*
    Template Name: Tìm kiếm sản phẩm trên amazon
    */
    get_header();
?>

<style>
    .loading-field-component__loading-img {
        background: url(<?php echo MZ_PLUGIN_URI ?>/assets/images/loadingNew.gif) no-repeat center 100% !important;
        animation: none !important;
    }
</style>

<div id="mz-search-amazon-product__main-content">
    <div class="mz-search-amazon-product__container">
        <div id="content-area" class="clearfix">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="entry-content">
                    <?php the_content(); ?>
                    <div class="search-result-layout" style="margin-top: 20px">
                        <section class="js-search-result-block search-result-block">
                            <div class="container">
                                <div class="search-result-block__head">
                                    <div class="search-result-block__title-col">
                                        <span>Tìm kiếm từ khóa "<span class="text--color-blue"><?php echo apply_filters('mz_filter_keyword', '') ?></span>" thuộc Amazon Mỹ</span>
                                        <span class="search-result-block__quantity-field">/ Tìm thấy <b class="js-total-item">0</b> sản phẩm tại Amazon Mỹ</span>
                                    </div>
                                    <div class="search-result-block__control-col">
                                        <a href="<?php echo apply_filters('mz_generate_category_uri', MZ_COUNTRY_US) ?>" class="my-btn -btn-grd-bg -btn-pill -btn-sm">
                                            Xem tất cả
                                            <i class="fal fa-angle-double-right margin--left-5px"></i></a></div>
                                </div>
                                <div id="search-result-block--us" class="swiper-container swiper-container-horizontal">
                                    <div class="js-search-result-block__content--us swiper-wrapper" style="">
                                        <div class="js-loading-block loading-field-component">
                                            <div class="loading-field-component__inner">
                                                <div class="loading-field-component__loading-field">
                                                    <div class="loading-field-component__loading-img"></div>
                                                </div>

                                                <div class="loading-field-component__title">Đang tìm kiếm thông tin sản phẩm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-scrollbar">
                                        <div class="swiper-scrollbar-drag" style=""></div>
                                    </div>
                                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                                </div>
                                <div id="search-result-block--us__prev-btn" class="search-result-block__prev-btn" aria-label="Previous slide" aria-disabled="true"></div>
                                <div id="search-result-block--us__next-btn" class="search-result-block__next-btn" aria-label="Next slide" aria-disabled="false"></div>
                            </div>
                        </section>
                        <section class="js-search-result-block search-result-block">
                            <div class="container">
                                <div class="search-result-block__head">
                                    <div class="search-result-block__title-col">
                                        <span>Tìm kiếm từ khóa "<span class="text--color-blue"><?php echo apply_filters('mz_filter_keyword', '') ?></span>" thuộc Amazon Nhật</span>
                                        <span class="search-result-block__quantity-field">/ Tìm thấy <b class="js-total-item">0</b> sản phẩm tại Amazon Nhật</span>
                                    </div>
                                    <div class="search-result-block__control-col">
                                        <a href="<?php echo apply_filters('mz_generate_category_uri', MZ_COUNTRY_JP) ?>" class="my-btn -btn-grd-bg -btn-pill -btn-sm">
                                            Xem tất cả
                                            <i class="fal fa-angle-double-right margin--left-5px"></i></a></div>
                                </div>
                                <div id="search-result-block--jp" class="swiper-container swiper-container-horizontal">
                                    <div class="js-search-result-block__content--jp swiper-wrapper" style="">
                                        <div class="js-loading-block loading-field-component">
                                            <div class="loading-field-component__inner">
                                                <div class="loading-field-component__loading-field">
                                                    <div class="loading-field-component__loading-img"></div>
                                                </div>

                                                <div class="loading-field-component__title">Đang tìm kiếm thông tin sản phẩm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-scrollbar">
                                        <div class="swiper-scrollbar-drag" style=""></div>
                                    </div>
                                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                                </div>
                                <div id="search-result-block--jp__prev-btn" class="search-result-block__prev-btn" aria-label="Previous slide" aria-disabled="true"></div>
                                <div id="search-result-block--jp__next-btn" class="search-result-block__next-btn" aria-label="Next slide" aria-disabled="false"></div>
                            </div>
                        </section>
                        <section class="js-search-result-block search-result-block">
                            <div class="container">
                                <div class="search-result-block__head">
                                    <div class="search-result-block__title-col">
                                        <span>Tìm kiếm từ khóa "<span class="text--color-blue"><?php echo apply_filters('mz_filter_keyword', '') ?></span>" thuộc Amazon Đức</span>
                                        <span class="search-result-block__quantity-field">/ Tìm thấy <b class="js-total-item">0</b> sản phẩm tại Amazon Đức</span>
                                    </div>
                                    <div class="search-result-block__control-col">
                                        <a href="<?php echo apply_filters('mz_generate_category_uri', MZ_COUNTRY_DE) ?>" class="my-btn -btn-grd-bg -btn-pill -btn-sm">
                                            Xem tất cả
                                            <i class="fal fa-angle-double-right margin--left-5px"></i></a></div>
                                </div>
                                <div id="search-result-block--de" class="swiper-container swiper-container-horizontal">
                                    <div class="js-search-result-block__content--de swiper-wrapper" style="">
                                        <div class="js-loading-block loading-field-component">
                                            <div class="loading-field-component__inner">
                                                <div class="loading-field-component__loading-field">
                                                    <div class="loading-field-component__loading-img"></div>
                                                </div>

                                                <div class="loading-field-component__title">Đang tìm kiếm thông tin sản phẩm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-scrollbar">
                                        <div class="swiper-scrollbar-drag" style=""></div>
                                    </div>
                                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                                </div>
                                <div id="search-result-block--de__prev-btn" class="search-result-block__prev-btn" aria-label="Previous slide" aria-disabled="true"></div>
                                <div id="search-result-block--de__next-btn" class="search-result-block__next-btn" aria-label="Next slide" aria-disabled="false"></div>
                            </div>
                        </section>
                        <section class="js-search-result-block search-result-block">
                            <div class="container">
                                <div class="search-result-block__head">
                                    <div class="search-result-block__title-col">
                                        <span>Tìm kiếm từ khóa "<span class="text--color-blue"><?php echo apply_filters('mz_filter_keyword', '') ?></span>" thuộc Amazon Anh</span>
                                        <span class="search-result-block__quantity-field">/ Tìm thấy <b class="js-total-item">0</b> sản phẩm tại Amazon Anh</span>
                                    </div>
                                    <div class="search-result-block__control-col">
                                        <a href="<?php echo apply_filters('mz_generate_category_uri', MZ_COUNTRY_UK) ?>" class="my-btn -btn-grd-bg -btn-pill -btn-sm">
                                            Xem tất cả
                                            <i class="fal fa-angle-double-right margin--left-5px"></i></a></div>
                                </div>
                                <div id="search-result-block--uk" class="swiper-container swiper-container-horizontal">
                                    <div class="js-search-result-block__content--uk swiper-wrapper" style="">
                                        <div class="js-loading-block loading-field-component">
                                            <div class="loading-field-component__inner">
                                                <div class="loading-field-component__loading-field">
                                                    <div class="loading-field-component__loading-img"></div>
                                                </div>

                                                <div class="loading-field-component__title">Đang tìm kiếm thông tin sản phẩm</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-scrollbar">
                                        <div class="swiper-scrollbar-drag" style=""></div>
                                    </div>
                                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                                </div>
                                <div id="search-result-block--uk__prev-btn" class="search-result-block__prev-btn" aria-label="Previous slide" aria-disabled="true"></div>
                                <div id="search-result-block--uk__next-btn" class="search-result-block__next-btn" aria-label="Next slide" aria-disabled="false"></div>
                            </div>
                        </section>
                    </div>
                </div> <!-- .entry-content -->

            </article> <!-- .et_pb_post -->
        </div> <!-- #content-area -->
    </div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>
