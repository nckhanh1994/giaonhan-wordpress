<?php
    /*
    Template Name: Tìm kiếm sản phẩm amazon theo quốc gia
    */
    get_header();

?>

<style>
    .category-layout__main-col {
        width: 100%;
    }

    .category-block .product-panel-wrap__col {
        height: 350px !important;
    }

    .product-panel__feature-list li:before {
        background: url(https://st-fe-v2.fado.vn/frontend/main-page/desktop/v1/dist/images/svg/check-circle-grd.svg?v=1557843929014) no-repeat 50%;
    }
</style>

<div id="mz-search-amazon-product__main-content">
    <div class="mz-search-amazon-product__container">
        <div id="content-area" class="clearfix">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="entry-content">
                    <?php the_content(); ?>
                    <div class="category-layout">
                        <div class="container category-layout__container">
                            <div class="category-layout__main-col">
                                <?php echo do_action('mz_get_product_list') ?>
                            </div>
                        </div>
                    </div>
                </div> <!-- .entry-content -->

            </article> <!-- .et_pb_post -->
        </div> <!-- #content-area -->
    </div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>
