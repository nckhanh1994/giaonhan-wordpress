<?php
    /*
    Template Name: Tìm kiếm sản phẩm amazon theo quốc gia
    */
    get_header();

?>

<style>
    .container {
        width: 90%;
        max-width: 1600px;
    }
</style>

<div id="mz-search-amazon-product__main-content">
    <div class="mz-search-amazon-product__container">
        <div id="content-area" class="clearfix">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="entry-content">
                    <?php the_content(); ?>
                    <div id="app"></div>
                </div> <!-- .entry-content -->

            </article> <!-- .et_pb_post -->
        </div> <!-- #content-area -->
    </div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>
