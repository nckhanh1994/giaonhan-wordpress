<?php
    
    /*
    Template Name: Chi tiết sản phẩm
    */
    get_header();
?>

<style>
    .pd-block__price-info-item__lbl {
        flex: 0 0 40px !important;
    }

    .pd-block__change-price-btn {
        flex: 0 0 100% !important;
    }

    .product-detail-block .price-box {
        border: 1px solid #ddd;
        margin-bottom: 15px
    }

    .product-detail-block .price-box .box-head {
        background: url() center;
        background-size: cover;
        padding: 30px 30px 20px 25px;
    }

    .product-detail-block .price-box .box-head p {
        margin-bottom: 8px
    }

    .product-detail-block .price-box .box-head .main-price {
        font-size: 28px;
        line-height: 1;
        color: #b11e22;
        font-weight: 600
    }

    .product-detail-block .price-box .box-head .sub-price span {
        color: #b11e22;
        font-size: 16px;
        font-weight: 600
    }

    .product-detail-block .price-box .box-main {
        border-top: 1px solid #ddd;
        padding: 15px;
        display: none
    }

    .product-detail-block .price-box .box-main .price-tb {
        border: 1px solid #e5e5e5;
        width: 100%
    }

    .product-detail-block .price-box .box-main .price-tb td {
        padding: 7px 10px;
        border: 1px solid #e5e5e5;
        white-space: nowrap
    }

    .product-detail-block .price-box .box-main .price-tb td.val {
        color: #b11e22
    }

    .product-detail-block .price-box .box-main .price-tb tr:nth-of-type(odd) td {
        background: #f7f7f7
    }

    .product-detail-block .price-box .box-foot {
        padding: 10px 15px;
        cursor: pointer;
        border-top: 1px solid #ddd;
        color: #006cff
    }

    .product-detail-block .price-box .box-foot:hover {
        background: #f7f7f7
    }

    .sweet-alert p {
        background-color: #FEFAE3;
        padding: 17px;
        border: 1px solid #F0E1A1;
        display: block;
        margin: 22px;
        text-align: center;
        color: #61534e;
    }

    .pd-block__change-price-btn {
        flex: 0 0 50%;
    }

    .-svg-shop {
        background-image: url(/wp-content/plugins/miczone/assets/images/svg/shop.svg?v=1557893413087) !important;
    }
</style>

<div id="mz-search-amazon-product__main-content">
    <div class="mz-search-amazon-product__container">
        <div id="content-area" class="clearfix">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="entry-content">
                    <?php the_content(); ?>

                    <div class="pd-layout" style="padding: 20px;">
                        <?php do_action('mz_render_product_detail') ?>
                    </div>
                </div> <!-- .entry-content -->

            </article> <!-- .et_pb_post -->
        </div> <!-- #content-area -->
    </div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>
