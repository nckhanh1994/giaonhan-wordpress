<?php
    /**
     * Fires after the main content, before the footer is output.
     *
     * @since ??
     */
    do_action('et_after_main_content');
    
    if (has_action('mz_search_amazon_product_script'))
        do_action('mz_search_amazon_product_script');
    
    if ('on' == et_get_option('divi_back_to_top', 'false')) : ?>

        <span class="et_pb_scroll_top et-pb-icon"></span>
    
    <?php endif;
    
    if (!is_page_template('page-template-blank.php')) : ?>

        <footer id="main-footer">
            <?php get_sidebar('footer'); ?>
            
            
            <?php
                if (has_nav_menu('footer-menu')) : ?>

                    <div id="et-footer-nav">
                        <div class="container">
                            <?php
                                wp_nav_menu([
                                    'theme_location' => 'footer-menu',
                                    'depth'          => '1',
                                    'menu_class'     => 'bottom-nav',
                                    'container'      => '',
                                    'fallback_cb'    => '',
                                ]);
                            ?>
                        </div>
                    </div> <!-- #et-footer-nav -->
                
                <?php endif; ?>

            <div id="footer-bottom">
                <div class="container clearfix">
                    <?php
                        if (false !== et_get_option('show_footer_social_icons', true)) {
                            get_template_part('includes/social_icons', 'footer');
                        }
                        
                        echo et_get_footer_credits();
                    ?>
                </div>    <!-- .container -->
            </div>
        </footer> <!-- #main-footer -->
        </div> <!-- #et-main-area -->
    
    <?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

</div> <!-- #page-container -->

<?php wp_footer(); ?>

<!--
<div class="icon_contact active">
<a href="tel:0903600247">
<i class="fa fa-phone"></i>
</a>
</div> -->

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v3.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[ 0 ];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
        attribution=setup_tool
        page_id="547719455719051"
        logged_in_greeting="Hi! Bạn cần tư vấn gì không ?"
        logged_out_greeting="Hi! Bạn cần tư vấn gì không ?">
</div>

</body>
</html>
