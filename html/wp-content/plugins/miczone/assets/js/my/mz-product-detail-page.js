(function ($) {
    var Object = {
        init() {
            window._GLOBAL = {
                strEncryptCartData: '',
                EL: {
                    ADD_TO_CART_BTN: $('.js-amazon__add-to-cart')
                }
            };
            window._EVENT = this.event;
            window._REQUEST = this.request;
            window._FUNCTION = this.function;
            window._PLUGIN = this.plugin;

            this.event.init();
            this.request.init();
            this.function.init();
            this.plugin.init();
        },
        event: {
            init() {
                this.addToCart();
            },
            addToCart() {
                _GLOBAL.EL.ADD_TO_CART_BTN.on('click', function (e) {
                    e.preventDefault();

                    if (!_GLOBAL.strEncryptCartData)
                        return alert('Từ từ đợi load xong cái đã !');

                    var $that = $(this);
                    var $loadingEl = $that.find('i');
                    var $quantityEl = $('.js-amazon-quantity');
                    var quantity = !isNaN(parseInt($quantityEl.val())) ? parseInt($quantityEl.val()) : 1;

                    // TODO:
                    $.ajax({
                        type: "POST",
                        url: ajaxOption.ajaxURL,
                        cache: false,
                        data: {
                            action: 'add_to_cart',
                            type: 'product',
                            itemLink: ajaxOption.itemLink,
                            itemQuantity: quantity,
                            lang: ajaxOption.lang,
                            strEncryptCartData: _GLOBAL.strEncryptCartData
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $loadingEl.removeClass('fal fa-cart-plus').addClass('fa fa-spinner fa-spin');
                        },
                        error: function () {
                            $loadingEl.removeClass('fa fa-spinner fa-spin').addClass('fal fa-cart-plus');
                        }
                    }).done(function (res) {
                        $loadingEl.removeClass('fa fa-spinner fa-spin').addClass('fal fa-cart-plus');
                        if (!res.success)
                            return alert('Thêm giỏ hàng thất bại !');

                        return alert('Thêm giỏ hàng thành công !');
                    });
                })
            }
        },
        request: {
            init() {
                this.getFinalPriceRequest();
            },
            getFinalPriceRequest() {
                $.ajax({
                    type: "POST",
                    url: ajaxOption.ajaxURL,
                    data: {
                        action: 'get_final_price',
                        lang: ajaxOption.lang,
                        strRequestParam: ajaxOption.strEncryptFinalPriceParam
                    },
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    error: function () {

                    }
                }).done(function (res) {
                    if (res.success != 1)
                        return;

                    var data = res.data.arrFinalPriceData;
                    _GLOBAL.strEncryptCartData = res.data.strEncryptCartData;

                    _FUNCTION.bindFinalPriceToDOM(data);
                });
            }
        },
        function: {
            init() {

            },
            bindFinalPriceToDOM(data) {
                var $blockFinalPriceEle = $('.js-amazon__block-final-price');
                var $blockLocalTaxEle = $('.js-amazon__block-local-tax');
                var $blockShippingWeightEle = $('.js-amazon__block-shipping-weight');
                var $blockShippingFeeEle = $('.js-amazon__block-shipping-fee');
                var $blockImportFeeEle = $('.js-amazon__block-import-fee');
                var $blockOtherChargeFeeEle = $('.js-amazon__block-other-charge-fee');

                var finalPrice = !isNaN(parseFloat(data.finalPrice.currentPrice.priceAfterTaxInVN)) ? parseFloat(data.finalPrice.currentPrice.priceAfterTaxInVN) : 0;
                var unitPrice = !isNaN(parseFloat(data.arrTax.unitPrice)) ? parseFloat(data.arrTax.unitPrice) : 0;
                var localTax = !isNaN(parseFloat(data.arrTax.localTax)) ? parseFloat(data.arrTax.localTax) : 0;
                var importFee = !isNaN(parseFloat(data.arrTax.importFee)) ? parseFloat(data.arrTax.importFee) : 0;
                var shippingFee = !isNaN(parseFloat(data.arrTax.shippingFee)) ? parseFloat(data.arrTax.shippingFee) : 0;
                var shippingWeight = !isNaN(parseFloat(data.arrTax.shippingWeigh)) ? parseFloat(data.arrTax.shippingWeigh) : 0;
                var otherChargeFee = !isNaN(parseFloat(data.arrTax.otherChargeFee)) ? parseFloat(data.arrTax.otherChargeFee) : 0;


                var finalPrice = Math.round(finalPrice * 100) / 100;
                var unitPrice = Math.round(unitPrice * 100) / 100;
                var localTax = Math.round(localTax * 100) / 100;
                var importFee = Math.round(importFee * 100) / 100;
                var shippingFee = Math.round(shippingFee * 100) / 100;
                var shippingWeight = Math.round(shippingWeight * 100) / 100;
                var otherChargeFee = Math.round(otherChargeFee * 100) / 100;

                $('.js-amazon__final-price').text(finalPrice);
                $('.js-amazon__block-final-price').find('.js-amazon__value').text(finalPrice);
                $('.js-amazon__block-local-tax').find('.js-amazon__value').text(localTax);
                $('.js-amazon__block-unit-price').find('.js-amazon__value').text(unitPrice);
                $('.js-amazon__block-import-fee').find('.js-amazon__value').text(importFee);
                $('.js-amazon__block-shipping-fee').find('.js-amazon__value').text(shippingFee);
                $('.js-amazon__block-shipping-weight').find('.js-amazon__value').text(shippingWeight);
                $('.js-amazon__block-other-charge-fee').find('.js-amazon__value').text(otherChargeFee);
            }
        },
        plugin: {
            init() {
                this.swiper();
                this.lazyload();
            },
            swiper() {
                var elThumbSwiperContainer = document.getElementById('pd-image-block__thumb-swiper-container');
                var arrElThumbGalleryItem = elThumbSwiperContainer.querySelectorAll('.pd-image-block__thumb-gallery-item');
                var i = 0;
                var i2 = 0;

                // Thumbnail slider
                var objThumbSlider = new Swiper(elThumbSwiperContainer, {
                    loop: false,
                    slidesPerView: 4,
                    spaceBetween: 10,
                    slidesPerGroup: 4,
                    simulateTouch: false,
                    navigation: {
                        nextEl: document.getElementById('pd-image-block__next-btn'),
                        prevEl: document.getElementById('pd-image-block__prev-btn'),
                        disabledClass: 'is-disabled',
                    },
                });

                // Main product image slider
                var objSlider = new Swiper(document.getElementById('pd-image-block__swiper-container'), {
                    slidesPerView: 1,
                    spaceBetween: 20,
                    simulateTouch: false,
                    on: {
                        slideChangeTransitionStart() {
                            objThumbSlider.slideTo(objSlider.activeIndex);
                        },
                    },
                });

                // change slider when click thumb image
                let lengthElThumbGalleryItem = arrElThumbGalleryItem.length;
                for (i = 0; i < lengthElThumbGalleryItem; i++) {
                    arrElThumbGalleryItem[ i ].addEventListener('click', function () {
                        for (i2 = 0; i2 < lengthElThumbGalleryItem; i2++) {
                            arrElThumbGalleryItem[ i2 ].classList.remove('is-active');
                        }

                        this.classList.add('is-active');
                        objSlider.slideTo(this.dataset.target);
                    });
                }
            },
            lazyload() {
                $(".lazyload").lazyload({
                    event: "sporty"
                });
            }
        },
    }

    $(document).ready(function () {
        Object.init();
    });
})(jQuery);