(function ($) {
    var Object = {
        init() {
            window._GLOBAL = {};
            window._EVENT = this.event;
            window._REQUEST = this.request;
            window._FUNCTION = this.function;
            window._PLUGIN = this.plugin;

            this.event.init();
            this.request.init();
            this.function.init();
            this.plugin.init();
        },
        event: {
            init() {

            }
        },
        request: {
            init() {
                _FUNCTION.getMultipleLastPrice(ajaxOption.lang, ajaxOption.arrGroupAsinMerchant);
            },
            getMultipleLastPriceRequest: function (intLoadNumber, lang, asinMerchant) {
                $.ajax({
                    type: "POST",
                    url: ajaxOption.ajaxURL,
                    data: {
                        action: 'get_last_price',
                        lang: lang,
                        asinMerchant: asinMerchant
                    },
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    error: function () {

                    }
                }).done(function (res) {
                    if (res.success != 1)
                        return;

                    var data = res.data.arrProductList;

                    var $searchResultLangBlock = $('.js-search-result-block__content--' + lang);

                    $searchResultLangBlock.find('.js-product-block:not(.is-checked)').each(function (index, ele) {
                        var $ele = $(ele);
                        var asin = $ele.data('asin');

                        // Asin này không data => next
                        if ($.isEmptyObject(data) || typeof data[ asin ] === "undefined" || $.isEmptyObject(data[ asin ])) {
                            return;
                        }

                        $ele.addClass('is-checked');

                        if (data[ asin ].totalPrice) {
                            $ele.find(".js-price").html(data[ asin ].currency + data[ asin ].totalPrice);
                        } else {
                            $ele.find(".js-price").html(" Contact");
                        }

                        $ele.removeClass('is-loading-price');
                    })

                    if (intLoadNumber == 0) {
                        $searchResultLangBlock.find('.js-product-block:not(.is-checked)').each(function (index, ele) {
                            var $ele = $(ele);
                            $ele.removeClass('is-loading-price');
                            $ele.find(".js-price").html(" Contact");
                        })
                    }
                });
            }
        },
        function: {
            init() {

            },
            getMultipleLastPrice: function (lang, arrGroupAsinMerchant) {
                var intLoadNumber = arrGroupAsinMerchant.length // Số nhóm asin đã tải
                $.each(arrGroupAsinMerchant, function (i, asinMerchant) {
                    setTimeout(function () {
                        _REQUEST.getMultipleLastPriceRequest(intLoadNumber, lang, asinMerchant);
                    }, 50 * i);
                })
            }
        },
        plugin: {
            init() {
                this.lazyload();
            },
            lazyload() {
                $(".lazyload").lazyload({
                    event: "sporty"
                });
            }
        },
    }

    $(document).ready(function () {
        Object.init();
    });
})(jQuery);