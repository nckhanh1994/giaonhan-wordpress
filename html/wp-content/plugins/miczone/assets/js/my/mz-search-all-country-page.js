(function ($) {
    var Object = {
        init() {
            window._GLOBAL = {};
            window._EVENT = this.event;
            window._REQUEST = this.request;
            window._FUNCTION = this.function;
            window._PLUGIN = this.plugin;

            this.event.init();
            this.request.init();
            this.function.init();
            this.plugin.init();
        },
        event: {
            init() {

            }
        },
        request: {
            init() {
                $.each(ajaxOption.arrLang, function (index, lang) {
                    _REQUEST.getProductListRequest(lang);
                })
            },
            getProductListRequest(lang) {
                var $searchResultLangBlock = $('.js-search-result-block__content--' + lang);
                var $searchResultBlock = $searchResultLangBlock.closest('.js-search-result-block');

                $.ajax({
                    type: "POST",
                    url: ajaxOption.ajaxURL,
                    data: {
                        action: 'get_product_list',
                        lang: lang,
                        requestParam: ajaxOption.requestParam
                    },
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    error: function () {

                    }
                }).done(function (res) {
                    if (res.success != 1)
                        return;

                    var data = res.data;

                    $searchResultLangBlock.html(data.html);
                    $searchResultBlock.find('.js-total-item').text(data.arrResultCount.totalResults);

                    _PLUGIN.enableSwiper(
                        document.getElementById('search-result-block--' + lang),
                        document.getElementById('search-result-block--' + lang + '__next-btn'),
                        document.getElementById('search-result-block--' + lang + '__prev-btn')
                    );

                    _FUNCTION.getMultipleLastPrice(lang, data.arrGroupAsinMerchant);
                });
            },
            getMultipleLastPriceRequest: function (intLoadNumber, lang, asinMerchant) {
                $.ajax({
                    type: "POST",
                    url: ajaxOption.ajaxURL,
                    data: {
                        action: 'get_last_price',
                        lang: lang,
                        asinMerchant: asinMerchant
                    },
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    error: function () {

                    }
                }).done(function (res) {
                    if (res.success != 1)
                        return;

                    var data = res.data.arrProductList;

                    var $searchResultLangBlock = $('.js-search-result-block__content--' + lang);

                    $searchResultLangBlock.find('.js-product-block:not(.is-checked)').each(function (index, ele) {
                        var $ele = $(ele);
                        var asin = $ele.data('asin');

                        // Asin này không data => next
                        if ($.isEmptyObject(data) || typeof data[ asin ] === "undefined" || $.isEmptyObject(data[ asin ])) {
                            return;
                        }

                        $ele.addClass('is-checked');

                        if (data[ asin ].totalPrice) {
                            $ele.find(".js-price").html(data[ asin ].currency + data[ asin ].totalPrice);
                        } else {
                            $ele.find(".js-price").html(" Contact");
                        }

                        $ele.removeClass('is-loading-price');
                    })

                    if (intLoadNumber == 0) {
                        $searchResultLangBlock.find('.js-product-block:not(.is-checked)').each(function (index, ele) {
                            var $ele = $(ele);
                            $ele.removeClass('is-loading-price');
                            $ele.find(".js-price").html(" Contact");
                        })
                    }
                });
            }
        },
        function: {
            init() {

            },
            getMultipleLastPrice: function (lang, arrGroupAsinMerchant) {
                var intLoadNumber = arrGroupAsinMerchant.length // Số nhóm asin đã tải
                $.each(arrGroupAsinMerchant, function (i, asinMerchant) {
                    setTimeout(function () {
                        _REQUEST.getMultipleLastPriceRequest(intLoadNumber, lang, asinMerchant);
                    }, 50 * i);
                })
            }
        },
        plugin: {
            init() {
            },
            enableSwiper(ele, nextBtn, prevBtn) {
                var objSlider = new Swiper(ele, {
                    loop: false,
                    slidesPerView: 5,
                    spaceBetween: 10,
                    slidesPerGroup: 5,
                    simulateTouch: true,
                    navigation: {
                        nextEl: nextBtn,
                        prevEl: prevBtn,
                        disabledClass: 'is-disabled',
                    },
                });
            }
        },
    }

    $(document).ready(function () {
        Object.init();
    });
})(jQuery);