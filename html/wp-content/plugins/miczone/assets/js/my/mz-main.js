(function ($) {
    window.mz = window.mz || {};

    mz.MiczoneSearchAmazonProduct = {
        uri: '/tim-kiem-san-pham-amazon',
        getURI: function (keyword) {
            return this.uri + '/?rh=k:' + keyword + '&keywords=' + keyword;
        }
    };

    // your standard jquery code goes here with $ prefix
    // best used inside a page with inline code,
    // or outside the document ready, enter code here
    $(document).ready(function () {
        var $elSearchField = $('.et-search-field');
        $elSearchField.on('keypress', function (e) {
            var keyCode = e.which;
            var keyword = $.trim($elSearchField.val())

            if (keyCode == 13) {
                window.location.href = mz.MiczoneSearchAmazonProduct.getURI(keyword);
                return false;
            }

        });
    })
})(jQuery);