// Require
const MODULE_CONFIG = require('./src/pages');
// Local Plugins
const path = require('path')
const DATE = new Date()
const VueLoaderPlugin = require('vue-loader/lib/plugin');
// Init
const WEB_PATH = path.resolve(__dirname)
const WEB_DIST_PATH = path.resolve(__dirname, '')
const WEB_SRC_PATH = path.resolve(__dirname, 'src')
const NODE_MODULES_PATH = path.resolve(__dirname, 'node_modules')

const WEB_PATHS = {
    dist: {
        js: WEB_DIST_PATH,
    },
    src: {
        js: WEB_SRC_PATH + '/js',
        vue: WEB_SRC_PATH + '/vue',
        vuePage: WEB_SRC_PATH + '/pages'
    }
}

const WEB_CONFIG = {
    cacheVer: DATE.getTime(),
    appPath: WEB_PATH,
    nodeModulesPath: NODE_MODULES_PATH,
    distPath: WEB_DIST_PATH,
    srcPath: WEB_SRC_PATH,
    webPaths: WEB_PATHS,
    webpackConfig: {
        resolves: {
            extensions: [ '.js', '.jsx', '.vue' ],
            alias: {
                '@srcPath': WEB_SRC_PATH,
                '@jsPath': WEB_PATHS.src.js,
                '@vuePath': WEB_PATHS.src.vue,
                '@vuePagePath': WEB_PATHS.src.vuePage
            }
        },
        plugin: {
            providePlugins: {
                $: 'jquery',
                jQuery: 'jquery'
            }
        },
        // optimization: {
        //     splitChunks: {
        //         minChunks: 2,
        //         cacheGroups: {
        //             js: {
        //                 test: /\.(js)$/,
        //                 name: 'vendor',
        //                 chunks: 'all',
        //                 minSize: 350000,
        //                 minChunks: 4
        //             }
        //         }
        //     }
        // }
    }
}

const WEBPACK_CONFIG = {
    mode: 'development',
    entry: MODULE_CONFIG.entry,
    output: {
        path: WEB_CONFIG.webPaths.dist.js,
        filename: '[name].js',
    },
    resolve: WEB_CONFIG.webpackConfig.resolves,
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [
                    {
                        loader: 'vue-loader'
                    }
                ],
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                    }
                ]
            },
            {
                test: /\.css$/,
                loader: 'css-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                }
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
    devServer: {
        writeToDisk: true,
        disableHostCheck: true,
        port: 8080
    },
}

module.exports = WEBPACK_CONFIG