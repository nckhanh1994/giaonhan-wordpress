// Local Plugins
const path = require('path');
// Init
const OUTPUT_PATH = 'assets/js/my';
const WEB_PATH = path.resolve(__dirname)

const entry = {
    /** @auth */
    [ OUTPUT_PATH + '/mz-view-cart-page' ]: WEB_PATH + '/mz-view-cart-page/main.js',
    [ OUTPUT_PATH + '/mz-checkout-order-page' ]: WEB_PATH + '/mz-checkout-order-page/main.js',
}

module.exports = entry