import createRequest from '../utils/index'
import axios from 'axios'
import * as _ENUM from '../constants/enum'
import * as _CONSTANT from '../constants/index'

/**
 * @user - Thông tin đăng nhập
 * */
export const getCartDataRequest = (data) => createRequest(_CONSTANT.GET_CART_DATA, 'POST', data)
export const addToCartRequest = (data) => createRequest(_CONSTANT.ADD_TO_CART, 'POST', data)
export const updateCartItemRequest = (data) => createRequest(_CONSTANT.UPDATE_CART_ITEM, 'POST', data)
export const removeCartItemRequest = (data) => createRequest(_CONSTANT.REMOVE_CART_ITEM, 'POST', data)
export const changeProductTypeRequest = (data) => createRequest(_CONSTANT.CHANGE_PRODUCT_TYPE, 'POST', data)



