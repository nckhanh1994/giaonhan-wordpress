import axios from 'axios'
import * as _CONTSTANT from '../constants/index'

/** @axios */
export default (endpoint, method = 'GET', data) => {
    data.append('action', endpoint);
    // Send a POST request
    return axios({
        method: 'POST',
        url: `${_CONTSTANT.API_URL}`,
        data: data
    }).catch(err => {
        console.log(err)
    })
}