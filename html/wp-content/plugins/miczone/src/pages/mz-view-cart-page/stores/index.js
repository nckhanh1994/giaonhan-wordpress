import * as _API from '../apis/index'

class Store {
    constructor() {
        return {
            state: {
                layout: 'admin-layout',
                arrProductTypeList: {},
                arrCartItemList: {},
            },
            mutations: {
                getProductTypeList(state, data) {
                    state.arrProductTypeList = data
                },
                getCartData(state, data) {
                    state.arrCartItemList = data
                },
                addToCart(state, data) {
                    state.arrCartItemList.push(data)
                },
                updateCartItem(state, obj) {
                    state.arrCartItemList[ obj.index ].item_quantity = obj.quantity
                },
                removeCartItem(state, index) {
                    state.arrCartItemList.splice(index, 1)
                },
                changeProductType(state, obj) {
                    state.arrCartItemList.splice(obj.index, 1, obj.data)
                },
            },
            actions: {
                getCartData({ commit }) {
                    var formData = new FormData()
                    _API.getCartDataRequest(formData).then(res => {
                        var res = res.data
                        if (!res.success)
                            return 'Xử lý khi lấy thông tin giỏ hàng thất bại !';

                        var data = res.data;
                        console.log(data)

                        commit('getProductTypeList', data.arrProductTypeList)
                        commit('getCartData', data.arrCartData)
                    })
                },
                addToCart({ commit, getters }, payload) {
                    // Loading
                    payload.input.setAttribute('disabled', '')
                    payload.loadingEl.classList.remove('hide')
                    if (payload.$el)
                        payload.$el.classList.add('is-loading')

                    var formData = new FormData()
                    formData.append('type', 'cart');
                    formData.append('itemLink', payload.itemLink);

                    _API.addToCartRequest(formData).then(res => {
                        // Remove Loading
                        payload.input.removeAttribute('disabled', '')
                        payload.loadingEl.classList.add('hide')
                        if (payload.$el)
                            payload.$el.classList.remove('is-loading')

                        var res = res.data
                        if (!res.success)
                            return 'Xử lý khi add vào giỏ hàng thất bại !';

                        var data = res.data

                        if (data.type == 'add')
                            commit('addToCart', data.arrCartData)
                        else {
                            let { id, quantity } = data.arrCartData
                            let index = getters.getCartById(id)
                            commit('updateCartItem', {
                                index: index,
                                quantity: quantity
                            })
                        }
                    }).catch(() => {
                        // Remove Loading
                        payload.input.removeAttribute('disabled', '')
                        payload.loadingEl.classList.add('hide')
                        if (payload.$el)
                            payload.$el.classList.remove('is-loading')
                    })
                },
                updateCartItem({ commit }, payload) {
                    var formData = new FormData()
                    formData.append('id', payload.id)
                    formData.append('quantity', payload.quantity)

                    payload.$el.classList.add('is-loading')

                    _API.updateCartItemRequest(formData).then(res => {
                        payload.$el.classList.remove('is-loading')

                        var res = res.data
                        if (!res.success)
                            return 'Xử lý khi cập nhật sản phẩm thất bại !';

                        commit('updateCartItem', {
                            index: payload.index,
                            quantity: payload.quantity
                        })
                    })
                },
                removeCartItem({ commit, getters }, payload) {
                    var formData = new FormData()
                    formData.append('id', payload.id)

                    _API.removeCartItemRequest(formData).then(res => {
                        payload.$el.classList.remove('is-loading')

                        var res = res.data
                        if (!res.success)
                            return 'Xử lý khi xóa sản phẩm giỏ hàng thất bại !';

                        let index = getters.getCartById(payload.id)

                        commit('removeCartItem', index)
                    }).catch(() => {
                        payload.$el.classList.remove('is-loading')
                    })
                },
                changeProductType({ commit, getters }, payload) {
                    var formData = new FormData()
                    formData.append('id', payload.id)
                    formData.append('productTypeID', payload.productTypeID)

                    payload.$el.classList.add('is-loading')

                    _API.changeProductTypeRequest(formData).then(res => {
                        payload.$el.classList.remove('is-loading')

                        var res = res.data
                        if (!res.success)
                            return 'Xử lý khi xóa sản phẩm giỏ hàng thất bại !';

                        var data = res.data

                        commit('changeProductType', { index: payload.index, data: data.arrCartData })
                    }).catch(() => {
                        payload.$el.classList.remove('is-loading')
                    })
                }
            },
            getters: {
                getCartById: state => id => {
                    var index = 0
                    for (let i = 0; i < state.arrCartItemList.length; i++) {
                        let objCartItem = state.arrCartItemList[ i ]
                        if (id == objCartItem.id) {
                            index = i
                            break
                        }
                    }
                    return index
                },
                countItem(state) {
                    var countItem = 0
                    for (let i = 0; i < state.arrCartItemList.length; i++) {
                        countItem += parseInt(state.arrCartItemList[ i ].item_quantity)
                    }

                    return countItem
                },
                countItemFilter: state => lang => {
                    if (!lang)
                        return 0

                    var countItem = 0
                    for (let i = 0; i < state.arrCartItemList.length; i++) {
                        let objCartItem = state.arrCartItemList[ i ]
                        if (lang == objCartItem.item_lang) {
                            countItem += parseInt(objCartItem.item_quantity)
                        }
                    }

                    return countItem
                },
                layout(state) {
                    return state.layout
                }
            }
        }
    }
}

export default (new Store());


