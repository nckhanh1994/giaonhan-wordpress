export const BASE_URL = 'http://dev-fe.giaonhan.com';
export const API_VERSION = 'v1';
export const API_URL = BASE_URL + '/wp-admin/admin-ajax.php';

export const GET_CART_DATA = 'get_cart_data';
export const ADD_TO_CART = 'add_to_cart';
export const UPDATE_CART_ITEM = 'update_cart_item';
export const REMOVE_CART_ITEM = 'remove_cart_item';
export const CHANGE_PRODUCT_TYPE = 'change_product_type';