import createRequest from '../utils/index'
import axios from 'axios'
import * as _ENUM from '../constants/enum'
import * as _CONSTANT from '../constants/index'

export const getCheckoutDataRequest = (data) => createRequest(_CONSTANT.GET_CHECKOUT_DATA, 'POST', data)
export const getDistrictViaCityIDRequest = (data) => createRequest(_CONSTANT.GET_DISTRICT_VIA_CITY_ID, 'POST', data)
export const getWardViaDistrictIDRequest = (data) => createRequest(_CONSTANT.GET_WARD_VIA_DISTRICT_ID, 'POST', data)
export const checkoutOrderRequest = (data) => createRequest(_CONSTANT.CHECKOUT_ORDER, 'POST', data)



