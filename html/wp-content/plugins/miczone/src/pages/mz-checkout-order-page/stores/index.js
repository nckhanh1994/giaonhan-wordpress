import * as _API from '../apis/index'

class Store {
    constructor() {
        return {
            state: {
                layout: 'admin-layout',
                arrCartItemList: {},
                arrProductTypeList: {},
                arrCityList: {},
                arrDistrictList: {},
                arrWardList: {},
            },
            mutations: {
                getCartData(state, data) {
                    state.arrCartItemList = data
                },
                getProductTypeList(state, data) {
                    state.arrProductTypeList = data
                },
                getCityList(state, data) {
                    state.arrCityList = data
                },
                getDistrictList(state, data) {
                    state.arrDistrictList = data
                },
                getWardList(state, data) {
                    state.arrWardList = data
                }

            },
            actions: {
                getCheckoutData({ commit }) {
                    var formData = new FormData()

                    _API.getCheckoutDataRequest(formData).then(res => {
                        var res = res.data
                        if (!res.success)
                            return '';

                        var data = res.data;

                        commit('getCartData', data.arrCartData)
                        commit('getProductTypeList', data.arrProductTypeList)
                        commit('getCityList', data.arrCityList)
                    })
                },
                getDistrictViaCityID({ commit }, payload) {
                    var id = parseInt(payload.id)
                    if (isNaN(id)) {
                        commit('getDistrictList', {})
                        return '';
                    }

                    var formData = new FormData()
                    formData.append('city_id', id)

                    _API.getDistrictViaCityIDRequest(formData).then(res => {
                        var res = res.data
                        if (!res.success) {
                            commit('getDistrictList', {})
                            return '';
                        }

                        var data = res.data;

                        commit('getDistrictList', data.arrDistrictList)
                    })
                },
                getWardViaDistrictID({ commit }, payload) {
                    var id = parseInt(payload.id)
                    if (isNaN(id)) {
                        commit('getWardList', {})
                        return '';
                    }

                    var formData = new FormData()
                    formData.append('district_id', id)

                    _API.getWardViaDistrictIDRequest(formData).then(res => {
                        var res = res.data
                        if (!res.success) {
                            commit('getWardList', {})
                            return '';
                        }

                        var data = res.data;

                        commit('getWardList', data.arrWardList)
                    })
                },
                checkoutOrder({ commit }, payload) {
                    var formData = new FormData()
                    formData.append('userInfo', JSON.stringify(payload.userInfo))

                    _API.checkoutOrderRequest(formData).then(res => {
                        var res = res.data
                        if (!res.success) {

                            return '';
                        }

                        var data = res.data;

                        commit('getWardList', data.arrWardList)
                    })
                },
            },
            getters: {
                layout(state) {
                    return state.layout
                }
            }
        }
    }
}

export default (new Store());


