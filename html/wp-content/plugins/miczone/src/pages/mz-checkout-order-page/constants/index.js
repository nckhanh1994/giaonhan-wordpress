export const BASE_URL = 'http://dev-fe.giaonhan.com'
export const API_VERSION = 'v1'
export const API_URL = BASE_URL + '/wp-admin/admin-ajax.php'

export const GET_CHECKOUT_DATA = 'get_checkout_data'
export const CHECKOUT_ORDER = 'checkout_order'
export const GET_DISTRICT_VIA_CITY_ID = 'get_district_via_city_id'
export const GET_WARD_VIA_DISTRICT_ID = 'get_ward_via_district_id'
