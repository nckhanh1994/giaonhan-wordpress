import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import axios from 'axios'

import CheckoutOrderStore from './stores/index'

import App from './components/App'
import MzCheckoutOrderPage from './components/page/MzCheckoutOrderPage'

Vue.use(Vuex)
Vue.use(Vuetify)
Vue.use(VueRouter)

Vue.prototype.$http = axios;
const token = 'khanhnc';
if (token)
    Vue.prototype.$http.defaults.headers.common[ 'Authorization' ] = token;

const store = new Vuex.Store({
    strict: true,
    modules: {
        CheckoutOrderStore: CheckoutOrderStore
    }
})

const router = new VueRouter({
    mode: 'history',
    routes: [
        { name: 'miczone::view-cart-page', path: '/lien-he-dat-hang', component: MzCheckoutOrderPage },
    ]
});

(function ($) {
    $(document).ready(function () {
        const Object = (() => {
            const appEle = document.getElementById('app')

            const render = () => {
                new Vue({
                    router,
                    store,
                    render: h => h(App)
                }).$mount(appEle)
            }

            return {
                init() {
                    render()

                    $('.cart-block__cart-tab-item').off('click').on('click', function (e) {
                        var $that = $(this)
                        var lang = $that.data('lang')

                        $('.cart-block__cart-tab-item.is-active').removeClass('is-active')
                        $that.addClass('is-active')

                        if (!lang) {
                            $('.js-cart-block__product-tr').show()
                            return
                        }

                        $('.js-cart-block__product-tr').hide()
                        $('.js-cart-block__product-tr[data-lang="' + lang + '"]').show()
                    })
                },
            }
        })();

        Object.init();
    });
})(jQuery);
