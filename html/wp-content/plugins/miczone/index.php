<?php
    /**
     * Plugin Name: Miczone Search Amazon Product
     * Plugin URI: https://giaonhan247.com
     * Description: Tìm kiếm sản phẩm trên Amazon
     * Version: 1.0
     * Author: Nguyễn Công Khánh
     * Author URI: https://giaonhan247.com
     * License: GPLv2
     */
    
    
    /** @general */
    define('BASE_URL', 'http://dev-fe.giaonhan.com');
    define('SECRET_KEY', 'Http://@m@z0n247.VN/');
    define('MZ_PLUGIN_URI', rtrim(plugin_dir_url(__FILE__), '/'));
    define('MZ_PLUGIN_PATH', rtrim(plugin_dir_path(__FILE__), '/'));
    define('MZ_COUNTRY_US', 'us');
    define('MZ_COUNTRY_JP', 'jp');
    define('MZ_COUNTRY_DE', 'de');
    define('MZ_COUNTRY_UK', 'uk');
    /**  */
    
    /** @page */
    define('MZ_SEARCH_ALL_COUNTRY_PAGE', 'mz-search-all-country-page');
    define('MZ_SEARCH_COUNTRY_PAGE', 'mz-search-country-page');
    define('MZ_PRODUCT_DETAIL_PAGE', 'mz-product-detail-page');
    define('MZ_VIEW_CART_PAGE', 'mz-view-cart-page');
    define('MZ_CHECKOUT_ORDER_PAGE', 'mz-checkout-order-page');
    /**  */
    
    /** @route */
    define('MZ_SEARCH_ALL_COUNTRY_ROUTE_BEGIN', 'tim-kiem-san-pham-amazon');
    define('MZ_SEARCH_COUNTRY_ROUTE_BEGIN', 'tim-kiem-san-pham-amazon');
    define('MZ_VIEW_CART_ROUTE_BEGIN', 'gio-hang');
    define('MZ_CHECKOUT_ROUTE_BEGIN', 'lien-he-dat-hang');
    /**  */
    
    global $arrCountryCode, $arrConvertCountryCode;
    
    $arrConvertCountryCode = ['my' => 'us', 'nhat' => 'jp', 'duc' => 'de', 'anh' => 'uk'];
    
    $arrCountryCode = [
        MZ_COUNTRY_US => ['code' => 'us', 'txt' => 'Mỹ', 'slug' => 'my'],
        MZ_COUNTRY_JP => ['code' => 'jp', 'txt' => 'Nhật', 'slug' => 'nhat'],
        MZ_COUNTRY_DE => ['code' => 'de', 'txt' => 'Đức', 'slug' => 'duc'],
        MZ_COUNTRY_UK => ['code' => 'uk', 'txt' => 'Anh', 'slug' => 'anh'],
    ];
    
    
    /**  */
    function MiczonePlugin()
    {
        $pagePath = MZ_PLUGIN_PATH . '/includes/Page';
        
        $pageTemplate = rtrim(get_query_var('pagename'), '.php');
        /**  */
        $_instance = null;
        
        switch ($pageTemplate) {
            case MZ_SEARCH_ALL_COUNTRY_PAGE;
                require_once $pagePath . '/MzSearchAllCountryPage.php';
                
                /** @prepare */
                $param = $_GET;
                
                $intPage = !empty(is_numeric((int)$param['pages'])) ? (int)$param['pages'] : 1;
                
                $strKeyword = urlencode(trim($param['keywords']));
                if (empty($strKeyword))
                    wp_redirect('/');
                
                $_instance = new MzSearchAllCountryPage();
                $_instance
                    ->setPage($intPage)
                    ->setKeyword($strKeyword);
                
                add_filter('template_include', function () {
                    return get_template_directory() . '/' . MZ_SEARCH_ALL_COUNTRY_PAGE . '.php';
                });
                
                break;
            case MZ_SEARCH_COUNTRY_PAGE;
                global $arrConvertCountryCode;
                
                require_once $pagePath . '/MzSearchCountryPage.php';
                /** @prepare */
                $param = $_GET;
                
                $intPage = !empty((int)$param['pages']) ? (int)$param['pages'] : 1;
                
                $mzLang = $arrConvertCountryCode[ get_query_var('mz_lang') ];
                if (empty($mzLang) || !in_array($mzLang, ['us', 'jp', 'de', 'uk']))
                    wp_redirect('/');
                
                $strKeyword = urlencode(trim($param['keywords']));
                if (empty($strKeyword))
                    wp_redirect('/');
                
                $_instance = new MzSearchCountryPage();
                $_instance
                    ->setPage($intPage)
                    ->setLang($mzLang)
                    ->setKeyword($strKeyword);
                
                add_filter('template_include', function () {
                    return get_template_directory() . '/' . MZ_SEARCH_COUNTRY_PAGE . '.php';
                });
                
                break;
            case MZ_PRODUCT_DETAIL_PAGE;
                require_once $pagePath . '/MzProductDetailPage.php';
                
                /** @prepare */
                $param = $_GET;
                
                $mzSlug = get_query_var('mz_slug');
                if (empty($mzSlug))
                    wp_redirect('/');
                
                $mzAsin = get_query_var('mz_asin');
                if (empty($mzAsin))
                    wp_redirect('/');
                
                $merchantID = !empty($param['smid']) ? $param['smid'] : '';
                if (empty($merchantID))
                    wp_redirect('/');
                
                $lang = !empty(trim($param['lang'])) ? trim($param['lang']) : '';
                if (empty($lang) || !in_array($lang, ['us', 'jp', 'de', 'uk']))
                    wp_redirect('/');
                
                $query      = get_query_var('mz_query');
                $strKeyword = str_replace(['\'', '"', ',', ';', '<', '>'], '', $param['keywords']);
                
                $_instance = new MzProductDetailPage();
                $_instance
                    ->setLang($lang)
                    ->setAsin($mzAsin)
                    ->setMerchantID($merchantID)
                    ->setKeyword($strKeyword);
                
                add_filter('template_include', function () {
                    return get_template_directory() . '/' . MZ_PRODUCT_DETAIL_PAGE . '.php';
                });
                
                break;
            case MZ_VIEW_CART_PAGE:
                require_once $pagePath . '/MzViewCartPage.php';
                
                $_instance = new MzViewCartPage();
                
                add_filter('template_include', function () {
                    return get_template_directory() . '/' . MZ_VIEW_CART_PAGE . '.php';
                });
                
                break;
            case MZ_CHECKOUT_ORDER_PAGE:
                require_once $pagePath . '/MzCheckoutOrderPage.php';
                
                $_instance = new MzCheckoutOrderPage();
                
                add_filter('template_include', function () {
                    return get_template_directory() . '/' . MZ_CHECKOUT_ORDER_PAGE . '.php';
                });
                
                break;
            default:
                break;
        }
        
        if (isMiczonePage($pageTemplate)) {
            require_once $pagePath . '/MzPage.php';
            
            $result =
                (new MzPage($_instance))
                    ->run();
        }
    }
    
    function isMiczonePage($pageTemplate)
    {
        if (!in_array($pageTemplate, [MZ_SEARCH_ALL_COUNTRY_PAGE, MZ_SEARCH_COUNTRY_PAGE, MZ_PRODUCT_DETAIL_PAGE, MZ_VIEW_CART_PAGE, MZ_CHECKOUT_ORDER_PAGE]))
            return false;
        
        return true;
    }
    
    /**  */
    
    function MiczonePluginDeactive()
    {
        flush_rewrite_rules();
    }
    
    function MiczonePluginInit()
    {
        if (!session_id()) {
            session_start();
        }
    }
    
    function test()
    {
        flush_rewrite_rules();
    }
    
    function test2()
    {
        flush_rewrite_rules();
    }
    
    function MiczoneRewriteRules($arrRule)
    {
        $arrRule += [MZ_SEARCH_ALL_COUNTRY_ROUTE_BEGIN . '$' => 'index.php?pagename=' . MZ_SEARCH_ALL_COUNTRY_PAGE, 'top'];
        $arrRule += [MZ_SEARCH_COUNTRY_ROUTE_BEGIN . '-([a-z]+)' => 'index.php?pagename=' . MZ_SEARCH_COUNTRY_PAGE . '&mz_lang=$matches[1]', 'top'];
        $arrRule += ['([0-9a-zA-Z\-]+)/dp/([A-Z0-9]+)/([^/]*)' => 'index.php?pagename=' . MZ_PRODUCT_DETAIL_PAGE . '&mz_slug=$matches[1]&mz_asin=$matches[2]&mz_query=$matches[3]', 'top'];
        $arrRule += [MZ_VIEW_CART_ROUTE_BEGIN . '$' => 'index.php?pagename=' . MZ_VIEW_CART_PAGE, 'top'];
        $arrRule += [MZ_CHECKOUT_ROUTE_BEGIN . '$' => 'index.php?pagename=' . MZ_CHECKOUT_ORDER_PAGE, 'top'];
        
        return $arrRule;
    }
    
    function MiczoneQueryVars($arrQueryVar)
    {
        $arrQueryVar[] = 'mz_lang';
        $arrQueryVar[] = 'mz_slug';
        $arrQueryVar[] = 'mz_asin';
        $arrQueryVar[] = 'mz_query';
        
        return $arrQueryVar;
    }
    
    function MiczoneEnqueueScript()
    {
        $arrFile = [
            ''
        ];
        
        $dirPath = MZ_PLUGIN_URI . '/assets/js';
        wp_enqueue_script('mz-main.js', $dirPath . '/my/' . 'mz-main.js', ['jquery']);
    }
    
    register_activation_hook(__FILE__, 'test');
    register_deactivation_hook(__FILE__, 'test2');
    
    //add rewrite rules in case another plugin flushes rules
    add_action('init', 'MiczonePluginInit');
    //add rewrite rules in case another plugin flushes rules
    add_filter('rewrite_rules_array', 'MiczoneRewriteRules');
    //add plugin query vars (product_id) to wordpress
    add_filter('query_vars', 'MiczoneQueryVars');
    //add rewrite rules in case another plugin flushes rules
    add_action('wp_enqueue_scripts', 'MiczoneEnqueueScript');
    
    /**
     * ===================================================================================
     * =============================== AJAX ==============================================
     * ===================================================================================
     */
    
    /**  */
    require_once MZ_PLUGIN_PATH . '/includes/Ajax/MzAjaxSearch.php';
    $_instanceMzAjaxSearch = new MzAjaxSearch();
    
    add_action('wp_ajax_get_product_list', [&$_instanceMzAjaxSearch, 'getProductList']);
    add_action('wp_ajax_nopriv_get_product_list', [&$_instanceMzAjaxSearch, 'getProductList']);
    
    add_action('wp_ajax_get_last_price', [&$_instanceMzAjaxSearch, 'getLastPrice']);
    add_action('wp_ajax_nopriv_get_last_price', [&$_instanceMzAjaxSearch, 'getLastPrice']);
    /**  */
    
    /**  */
    require_once MZ_PLUGIN_PATH . '/includes/Ajax/MzAjaxProduct.php';
    $_instanceMzAjaxProduct = new MzAjaxProduct();
    
    add_action('wp_ajax_get_final_price', [&$_instanceMzAjaxProduct, 'getFinalPrice']);
    add_action('wp_ajax_nopriv_get_final_price', [&$_instanceMzAjaxProduct, 'getFinalPrice']);
    /**  */
    
    /**  */
    require_once MZ_PLUGIN_PATH . '/includes/Ajax/MzAjaxCart.php';
    $_instanceMzAjaxCart = new MzAjaxCart();
    
    add_action('wp_ajax_get_cart_data', [&$_instanceMzAjaxCart, 'getCartData']);
    add_action('wp_ajax_nopriv_get_cart_data', [&$_instanceMzAjaxCart, 'getCartData']);
    
    add_action('wp_ajax_add_to_cart', [&$_instanceMzAjaxCart, 'addToCart']);
    add_action('wp_ajax_nopriv_add_to_cart', [&$_instanceMzAjaxCart, 'addToCart']);
    
    add_action('wp_ajax_update_cart_item', [&$_instanceMzAjaxCart, 'updateCartItem']);
    add_action('wp_ajax_nopriv_update_cart_item', [&$_instanceMzAjaxCart, 'updateCartItem']);
    
    add_action('wp_ajax_remove_cart_item', [&$_instanceMzAjaxCart, 'removeCartItem']);
    add_action('wp_ajax_nopriv_remove_cart_item', [&$_instanceMzAjaxCart, 'removeCartItem']);
    
    add_action('wp_ajax_change_product_type', [&$_instanceMzAjaxCart, 'changeProductType']);
    add_action('wp_ajax_nopriv_change_product_type', [&$_instanceMzAjaxCart, 'changeProductType']);
    /**  */
    
    /**  */
    require_once MZ_PLUGIN_PATH . '/includes/Ajax/MzAjaxOrder.php';
    $_instanceMzAjaxOrder = new MzAjaxOrder();
    
    add_action('wp_ajax_get_checkout_data', [&$_instanceMzAjaxOrder, 'getCheckoutData']);
    add_action('wp_ajax_nopriv_get_checkout_data', [&$_instanceMzAjaxOrder, 'getCheckoutData']);
    
    add_action('wp_ajax_get_district_via_city_id', [&$_instanceMzAjaxOrder, 'getDistrictViaCityID']);
    add_action('wp_ajax_nopriv_get_district_via_city_id', [&$_instanceMzAjaxOrder, 'getDistrictViaCityID']);
    
    add_action('wp_ajax_get_ward_via_district_id', [&$_instanceMzAjaxOrder, 'getWardViaDistrictID']);
    add_action('wp_ajax_nopriv_get_ward_via_district_id', [&$_instanceMzAjaxOrder, 'getWardViaDistrictID']);
    
    add_action('wp_ajax_checkout_order', [&$_instanceMzAjaxOrder, 'checkoutOrder']);
    add_action('wp_ajax_nopriv_checkout_order', [&$_instanceMzAjaxOrder, 'checkoutOrder']);
    /**  */
    
    add_action('template_redirect', 'MiczonePlugin');
?>