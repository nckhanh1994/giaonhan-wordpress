<?php
    require_once MZ_PLUGIN_PATH . '/includes/Fado/Product.php';
    require_once MZ_PLUGIN_PATH . '/includes/Fado/Utils.php';
    
    class MzAjaxProduct
    {
        public function __construct()
        {
        
        }
        
        public function getFinalPrice()
        {
            if (isset($_POST)) {
                $strLang = !empty(trim($_POST['lang'])) ? trim($_POST['lang']) : '';
                if (empty($strLang) || !in_array($strLang, ['us', 'jp', 'de', 'uk'])) {
                    echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Lang not found !', 'data' => []]);
                    exit;
                }
                $strRequestParam = !empty($_POST['strRequestParam']) ? trim(strip_tags($_POST['strRequestParam'])) : '';
                $strRequestParam = trim(openssl_decrypt($strRequestParam, "AES-256-CBC", SECRET_KEY));
                
                $arrRequestParam = json_decode($strRequestParam, true);
                if (empty($arrRequestParam)) {
                    echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Param not found !', 'data' => []]);
                    exit;
                }
                
                $arrFadoData = (new Product())
                    ->setLang($strLang)
                    ->setParams(['getTax' => 1, 'arrRequestParam' => $arrRequestParam])
                    ->setAction('get-final-price')
                    ->getResult();
                
                $arrFinalPriceData = current($arrFadoData);
                // Bổ sung thuế nội địa
                $arrFinalPriceData['arrTax']['localTax'] = $arrFinalPriceData['arrTax']['estimatedOrderTotal'] - $arrFinalPriceData['arrTax']['unitPrice'];
                
                $arrTemp     = current($arrRequestParam);
                $arrCartData = [
                        'lang'         => $arrTemp['lang'],
                        'productName'  => $arrTemp['productName'],
                        'productImage' => $arrTemp['productImage'],
                        'productLink'  => $arrTemp['productLink'],
                        'asin'         => $arrTemp['asin'],
                        'merchantID'   => $arrTemp['currentMerchant']
                    ] + $arrFinalPriceData;
                
                $strEncryptCartData = trim(openssl_encrypt(json_encode($arrCartData), "AES-256-CBC", SECRET_KEY));
                
                unset($arrRequestParam);
                unset($arrTemp);
                
                echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => ['arrFinalPriceData' => $arrFinalPriceData, 'strEncryptCartData' => $strEncryptCartData]]);
                exit;
            }
            
            echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Phương thức này không được hổ trợ !', 'data' => []]);
            exit;
        }
    }

?>