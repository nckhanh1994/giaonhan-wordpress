<?php
    require_once MZ_PLUGIN_PATH . '/includes/Giaonhan/Cart.php';
    
    class MzAjaxCart
    {
        public function __construct()
        {
        
        }
        
        public function getCartData()
        {
            $arrParam = [
                'sessionID' => session_id()
            ];
            
            $result = (new Cart())
                ->setDebug(0)
                ->setAction('get_cart_data')
                ->setParams($arrParam)
                ->getResult();
            
            if (empty($result['success'])) {
                echo json_encode(['success' => -1, 'code' => -200, 'message' => '', 'data' => []]);
                exit;
            }
            
            echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => $result['data']]);
            exit;
        }
        
        public function addToCart()
        {
            $arrParam = array_merge($_POST, ['sessionID' => session_id()]);
            if (empty(trim($arrParam['itemLink']))) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Vui lòng kiểm tra lại Link sản phẩm của quý khách !', 'data' => []]);
                exit;
            }
            
            $result = (new Cart())
                ->setDebug(0)
                ->setAction('add_to_cart')
                ->setParams($arrParam)
                ->getResult();
            
            if (empty($result['success'])) {
                echo json_encode(['success' => -1, 'code' => -200, 'message' => 'Vui lòng kiểm tra lại Link sản phẩm của quý khách !', 'data' => []]);
                exit;
            }
            
            echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => $result['data']]);
            exit;
        }
        
        public function updateCartItem()
        {
            $arrParam = array_merge($_POST, ['sessionID' => session_id()]);
            if (empty(trim($arrParam['id']))) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Hệ thống đang gặp sự cố, xin quý khách vui lòng thử lại sau !', 'data' => []]);
                exit;
            }
            if (empty(trim($arrParam['quantity']))) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Hệ thống đang gặp sự cố, xin quý khách vui lòng thử lại sau !', 'data' => []]);
                exit;
            }
            
            $result = (new Cart())
                ->setAction('update_cart_item')
                ->setParams($arrParam)
                ->getResult();
            
            if (empty($result['success'])) {
                echo json_encode(['success' => -1, 'code' => -200, 'message' => '', 'data' => []]);
                exit;
            }
            
            echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => $result['data']]);
            exit;
        }
        
        public function removeCartItem()
        {
            $arrParam = array_merge($_POST, ['sessionID' => session_id()]);
            if (empty(trim($arrParam['id']))) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Hệ thống đang gặp sự cố, xin quý khách vui lòng thử lại sau !', 'data' => []]);
                exit;
            }
            
            $result = (new Cart())
                ->setAction('remove_cart_item')
                ->setParams($arrParam)
                ->getResult();
            
            if (empty($result['success'])) {
                echo json_encode(['success' => -1, 'code' => -200, 'message' => '', 'data' => []]);
                exit;
            }
            
            echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => $result['data']]);
            exit;
        }
        
        public function changeProductType()
        {
            $arrParam = array_merge($_POST, ['sessionID' => session_id()]);
            if (empty(trim($arrParam['id']))) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Hệ thống đang gặp sự cố, xin quý khách vui lòng thử lại sau !', 'data' => []]);
                exit;
            }
            
            $result = (new Cart())
                ->setAction('change_product_type')
                ->setParams($arrParam)
                ->getResult();
            
            if (empty($result['success'])) {
                echo json_encode(['success' => -1, 'code' => -200, 'message' => '', 'data' => []]);
                exit;
            }
            
            echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => $result['data']]);
            exit;
        }
    }

?>