<?php
    require_once MZ_PLUGIN_PATH . '/includes/Fado/Search.php';
    require_once MZ_PLUGIN_PATH . '/includes/Fado/Product.php';
    require_once MZ_PLUGIN_PATH . '/includes/Fado/Utils.php';
    
    class MzAjaxSearch
    {
        public function __construct()
        {
        
        }
        
        public function getProductList($isPaging = false)
        {
            if (isset($_POST)) {
                $param = $_POST;
                
                if (empty($param['lang'])) {
                    echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Lang not found !', 'data' => []]);
                    exit;
                }
                
                if (empty($param['requestParam'])) {
                    echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Param not found !', 'data' => []]);
                    exit;
                }
                
                $strRequestParam = !empty($param['requestParam']) ? trim(strip_tags($param['requestParam'])) : '';
                $arrRequestParam = json_decode(trim(openssl_decrypt($strRequestParam, "AES-256-CBC", SECRET_KEY)), true);
                if (empty($arrRequestParam)) {
                    echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Param not found !', 'data' => []]);
                    exit;
                }
                
                $lang = trim($param['lang']);
                
                $arrFadoData = (new Search())
                    ->setLang($lang)
                    ->setParams($arrRequestParam)
                    ->setAction('get-list')
                    ->getResult();
                
                $viewPath = MZ_PLUGIN_PATH . '/views/desktop/partial/search-result-block__content.php';
                $html     = renderPhpFile($viewPath, ['arrFadoData' => $arrFadoData, 'lang' => $lang], $arrReturn);
                
                if ($isPaging) {
                
                }
                
                echo json_encode(['success' => 1, 'code' => 200, 'message' => 'Lấy dữ liệu thành công !', 'data' => [
                    'arrResultCount'       => $arrFadoData['resultCount'],
                    'arrGroupAsinMerchant' => $arrReturn['arrGroupAsinMerchant'],
                    'html'                 => $html,
                ]]);
            }
            
            exit;
        }
        
        function getLastPrice()
        {
            if (isset($_POST)) {
                $strLang         = !empty($_POST['lang']) ? trim(strip_tags($_POST['lang'])) : 'us';
                $strAsinMerchant = !empty($_POST['asinMerchant']) ? trim(strip_tags($_POST['asinMerchant'])) : '';
                $strAsinMerchant = trim(openssl_decrypt($strAsinMerchant, "AES-256-CBC", SECRET_KEY));
                
                $arrAsinMerchant = explode(',', $strAsinMerchant);
                if (empty($arrAsinMerchant)) {
                    echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Asin not found !', 'data' => []]);
                    exit;
                }
                
                $arrFadoData = (new Product())
                    ->setLang($strLang)
                    ->setParams(['lang' => $strLang, 'arrAsinMerchant' => $arrAsinMerchant])
                    ->setAction('get-last-price')
                    ->getResult();
                
                $arrResponse = [];
                foreach ($arrFadoData as $strAsin => $arrItem) {
                    $asin = current(explode("-", $strAsin));
                    
                    $floatFinalPrice = !empty((float)$arrItem['finalPrice']['currentPrice']['priceAfterTaxInVN']) ? (float)$arrItem['finalPrice']['currentPrice']['priceAfterTaxInVN'] : 0;
                    
                    $arrResponse[ $asin ] = [
                        'totalPrice' => $floatFinalPrice,
                        'currency'   => '$'
                    ];
                }
                
                echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => ['arrProductList' => $arrResponse]]);
                exit;
            }
            
            echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Phương thức này không được hổ trợ !', 'data' => []]);
            exit;
        }
    }

?>