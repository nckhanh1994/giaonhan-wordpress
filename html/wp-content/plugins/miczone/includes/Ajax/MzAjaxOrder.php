<?php
    require_once MZ_PLUGIN_PATH . '/includes/Giaonhan/Order.php';
    
    class MzAjaxOrder
    {
        public function __construct()
        {
        
        }
        
        public function getCheckoutData()
        {
            $arrParam = [
                'sessionID' => session_id(),
            ];
            
            $result = (new Order())
                ->setDebug(0)
                ->setAction('get_checkout_data')
                ->setParams($arrParam)
                ->getResult();
            
            if (empty($result['success'])) {
                echo json_encode(['success' => -1, 'code' => -200, 'message' => '', 'data' => []]);
                exit;
            }
            
            echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => $result['data']]);
            exit;
        }
        
        public function getDistrictViaCityID()
        {
            $arrParam = array_merge($_POST, ['sessionID' => session_id()]);
            if (empty((int)$arrParam['city_id'])) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'City ID Not found !', 'data' => []]);
                exit;
            }
            
            $result = (new Order())
                ->setDebug(0)
                ->setAction('get_district_via_city_id')
                ->setParams($arrParam)
                ->getResult();
            
            if (empty($result['success'])) {
                echo json_encode(['success' => -1, 'code' => -200, 'message' => '', 'data' => []]);
                exit;
            }
            
            echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => $result['data']]);
            exit;
        }
        
        public function getWardViaDistrictID()
        {
            $arrParam = array_merge($_POST, ['sessionID' => session_id()]);
            if (empty((int)$arrParam['district_id'])) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'City ID Not found !', 'data' => []]);
                exit;
            }
            
            $result = (new Order())
                ->setDebug(0)
                ->setAction('get_ward_via_district_id')
                ->setParams($arrParam)
                ->getResult();
            
            if (empty($result['success'])) {
                echo json_encode(['success' => -1, 'code' => -200, 'message' => '', 'data' => []]);
                exit;
            }
            
            echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => $result['data']]);
            exit;
        }
        
        public function checkoutOrder()
        {
            $arrParam = array_merge($_POST, ['sessionID' => session_id()]);
            if (empty(trim($arrParam['userInfo']))) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'User Info Not found !', 'data' => []]);
                exit;
            }
            $jsonUserInfo = stripslashes(trim($arrParam['userInfo']));
            $arrUserInfo  = json_decode($jsonUserInfo, true);
            if (empty($arrUserInfo)) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'User Info is not correct !', 'data' => []]);
                exit;
            }
            
            $this->validateCheckoutOrder($arrUserInfo, $arrParam);
            
            $result = (new Order())
                ->setDebug(1)
                ->setAction('checkout_order')
                ->setParams($arrParam)
                ->getResult();
            
            if (empty($result['success'])) {
                echo json_encode(['success' => -1, 'code' => -200, 'message' => '', 'data' => []]);
                exit;
            }
            
            echo json_encode(['success' => 1, 'code' => 200, 'message' => '', 'data' => $result['data']]);
            exit;
        }
        
        /**
         * ===================================================================================
         * =============================== PRIVATE ===========================================
         * ===================================================================================
         */
        private function validateCheckoutOrder($arrParam = [], &$arrReturn)
        {
            $strReceiverName       = !empty($arrParam['receiver_name']) ? $arrParam['receiver_name'] : '';
            $strReceiverPhone      = !empty($arrParam['receiver_phone']) ? $arrParam['receiver_phone'] : '';
            $strReceiverEmail      = !empty($arrParam['receiver_email']) ? $arrParam['receiver_email'] : '';
            $strReceiverAddress    = !empty($arrParam['receiver_address']) ? $arrParam['receiver_address'] : '';
            $intReceiverCityID     = !empty($arrParam['receiver_city_id']) ? $arrParam['receiver_city_id'] : 0;
            $intReceiverDistrictID = !empty($arrParam['receiver_district_id']) ? $arrParam['receiver_district_id'] : 0;
            $intReceiveWardID      = !empty($arrParam['receiver_ward_id']) ? $arrParam['receiver_ward_id'] : 0;
            
            if (empty($strReceiverName)) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Vui lòng kiểm tra lại họ và tên !', 'data' => []]);
                exit;
            }
            
            if (empty($strReceiverAddress)) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Vui lòng kiểm tra lại địa chỉ !', 'data' => []]);
                exit;
            }
            
            if (empty($strReceiverEmail)) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Vui lòng kiểm tra lại số điện thoại !', 'data' => []]);
                exit;
            }
            
            if (empty($intReceiverCityID)) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Vui lòng kiểm tra lại Tỉnh / Thành !', 'data' => []]);
                exit;
            }
            
            if (empty($intReceiverDistrictID)) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Vui lòng kiểm tra lại Quận / Huyện !', 'data' => []]);
                exit;
            }
            
            if (empty($intReceiveWardID)) {
                echo json_encode(['success' => -1, 'code' => -10, 'message' => 'Vui lòng kiểm tra lại Phường / Xã !', 'data' => []]);
                exit;
            }
            
            $arrReturn = $arrReturn + [
                    'receiver_name'        => $strReceiverName,
                    'receiver_phone'       => $strReceiverPhone,
                    'receiver_email'       => $strReceiverEmail,
                    'receiver_address'     => $strReceiverAddress,
                    'receiver_city_id'     => $intReceiverCityID,
                    'receiver_district_id' => $intReceiverDistrictID,
                    'receiver_ward_id'     => $intReceiveWardID,
                ];
        }
    }

?>