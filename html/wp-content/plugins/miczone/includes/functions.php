<?php
    /**
     * ===================================================================================
     * =============================== FILTER HOOK ========================================
     * ===================================================================================
     */
    
    
    /**
     * ===================================================================================
     * =============================== ACTION HOOK =======================================
     * ===================================================================================
     */
    /**  */
    add_action('mz_enqueue_script_' . $page, function () use ($page) {
        global $mzParam;
        
        $strKeywords = urlencode(trim($mzParam['keywords']));
        $strRh       = urlencode(trim($mzParam['rh']));
        
        $arrParam = [
            'ajaxURL'      => admin_url('admin-ajax.php'),
            'requestParam' => openssl_encrypt(json_encode([
                'intPage' => 1,
                'keyword' => $strKeywords,
                'rh'      => $strRh,
                'sort'    => !empty($mzParam['sort']) ? '&sort=' . $mzParam['sort'] : ''
            ]), "AES-256-CBC", SECRET_KEY)
        ];
        
        wp_enqueue_script($page, get_template_directory_uri() . '/customize/' . $page . '/assets/js/my/index.js', ['jquery']);
        wp_localize_script($page, 'ajaxOption', $arrParam);
    });
    /**  */
?>