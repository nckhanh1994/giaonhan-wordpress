<?php
    include_once(__DIR__ . '/BotAbstract.php');
    
    class Product extends BotAbstract
    {
        public function getResult()
        {
            switch ($this->action) {
                case 'get-last-price';
                    $arrResult = $this->getLastPrice();
                    
                    $this->addKeyToEachElement('lang', $this->params['lang'], $arrResult);
                    
                    return $this
                        ->resetParams()
                        ->setParams(['arrRequestParam' => $arrResult])
                        ->setAction('get-final-price')
                        ->getResult();
                
                case 'get-detail':
                    return $this->getDetail();
                case 'get-final-price':
                    return $this->getFinalPrice();
                default:
                    return [];
            }
        }
        
        public function getLastPrice()
        {
            $arrRequestParam = [
                'lang' => !empty($this->params['lang']) ? $this->params['lang'] : 'us',
                'asin' => implode(',', $this->params['arrAsinMerchant'])
            ];
            
            $strURL = $this->setURL('/get-final-price.php?asin=' . $arrRequestParam['asin']);
            
            $output = $this->crawler($strURL, [], $this->arrHeader);
            $output = json_decode($output, true);
            
            return $output;
        }
        
        public function getFinalPrice()
        {
            $arrRequestParam = json_encode($this->params['arrRequestParam']);
            $strQuery        = !empty($this->params['getTax']) ? '?getTax=1' : '';
            
            $strURL = $this->setApiURL('/product/get-final-price' . $strQuery);
            
            $output = $this->crawler($strURL, $arrRequestParam, $this->arrHeader);
            $output = json_decode($output, true);
            
            if (empty($output['success']))
                return [];
            
            return $output['data'];
        }
        
        public function getDetail()
        {
            $strURL = $this->setURL('/product.php?asin=' . $this->params['asin'] . '&merchant=' . $this->params['merchantID']);
            
            $output = $this->crawler($strURL, [], $this->arrHeader);
            $output = json_decode($output, true);
            
            return $output;
        }
        
        /**
         * ===================================================================================
         * =============================== @PRIVATE ==========================================
         * ===================================================================================
         */
        private function addKeyToEachElement($newKey = '', $newValue = '', &$arrResult = [])
        {
            foreach ($arrResult as $key => $value)
                $arrResult[ $key ][ $newKey ] = $newValue;
        }
    }
