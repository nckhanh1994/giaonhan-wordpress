<?php
    
    class BotAbstract
    {
        const DEV_URL = 'http://staging.fado.vn/api/v3';
        const STAGING_URL = 'http://staging.fado.vn/api/v3';
        const PRODUCT_URL = 'https://fado.vn/api/v3';
        
        protected $botURL = 'http://bot.fado.vn';
        protected $apiURL = '';
        protected $lang = 'us';
        protected $action = '';
        protected $arrHeader = [];
        protected $params = [];
        
        
        public function __construct()
        {
            switch (APPLICATION_ENV) {
                case 'dev':
                    $this->apiURL = self::DEV_URL;
                    break;
                case 'staging':
                    $this->apiURL = self::STAGING_URL;
                    break;
                case 'production':
                default:
                    $this->apiURL = self::PRODUCT_URL;
                    break;
            }
            
            $this->setHeader();
        }
        
        /**
         * @param array $url
         * return @this
         */
        public function setURL($url = [])
        {
            $botVersion = 'v3';
            
            if ($this->lang == 'uk')
                $botVersion = 'v2';
            
            if ($this->lang == 'us')
                $botVersion = 'v5';
            
            $this->url = $this->botURL . '/amazon/' . $this->lang . '/' . $botVersion . $url;
            
            return $this->url;
        }
        
        /**
         * @param array $url
         * return @this
         */
        public function setApiURL($url = [])
        {
            $this->url = $this->apiURL . $url;
            
            return $this->url;
        }
        
        /**
         * @param string $lang
         * return @this
         */
        public function setLang($lang = [])
        {
            $this->lang = $lang;
            
            return $this;
        }
        
        /**
         * @param array $params
         * return @this
         */
        public function setParams($params = [])
        {
            $this->params = $params;
            
            return $this;
        }
        
        /**
         * @param array $action
         * return @this
         */
        public function setAction($action = [])
        {
            $this->action = $action;
            
            return $this;
        }
        
        /**
         * return @this
         */
        public function setHeader()
        {
            if (APPLICATION_ENV == 'staging' || APPLICATION_ENV == 'dev') {
                $arrHeader = [
                    "Authorization: Basic Z3Vlc3Q6MTIz",
                ];
            } else {
                $time       = time();
                $publicKey  = 'gi@onh@an2017';
                $encryptKey = 'fado@2018!$';
                
                $arrHeader = ["publicKey: $publicKey", "encryptKey: $encryptKey"];
            }
            
            $this->arrHeader = $arrHeader;
            
            return $this;
        }
        
        /**
         * @param string $strURL
         * @param array $arrData
         * @param array $arrHeaders
         * @param boolean $strCookiePath
         * return @curl_exec
         */
        public function crawler($strURL, $arrData = [], $arrHeaders = [], $strCookiePath = false)
        {
            $crawler = curl_init($strURL);
            
            if ($arrData) {
                curl_setopt($crawler, CURLOPT_POST, 1);
                curl_setopt($crawler, CURLOPT_POSTFIELDS, $arrData);
            }
            
            if ($arrHeaders) {
                curl_setopt($crawler, CURLOPT_HTTPHEADER, $arrHeaders);
            }
            if ($strCookiePath) {
                curl_setopt($crawler, CURLOPT_COOKIESESSION, true);
                curl_setopt($crawler, CURLOPT_COOKIEJAR, $strCookiePath);
                curl_setopt($crawler, CURLOPT_COOKIEFILE, $strCookiePath);
            }
            curl_setopt($crawler, CURLOPT_TIMEOUT, 30);
            curl_setopt($crawler, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($crawler, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($crawler, CURLOPT_DNS_CACHE_TIMEOUT, 0);
            curl_setopt($crawler, CURLOPT_MAXREDIRS, 5);
            curl_setopt($crawler, CURLOPT_ENCODING, "");
            curl_setopt($crawler, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($crawler, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($crawler, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            $data = curl_exec($crawler);
            curl_close($crawler);
            return $data;
        }
        
        public function resetParams()
        {
            $this->params = [];
            
            return $this;
        }
        
    }
