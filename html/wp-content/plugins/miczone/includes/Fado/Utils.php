<?php
    function generateSourceURL($strURL, $language = 'us', $isSearch = 0, $isCanonical = 0, $strParamURL = '')
    {
        parse_str(parse_url($strURL, PHP_URL_QUERY), $arrQuery);
        
        $lastNode = end($tmp);
        
        
        $strParams = str_replace(' ', '+', 'rh=' . $arrQuery['rh'] . '&keywords=' . $arrQuery['keywords']);
        $strParams .= $arrQuery['page'] ? '&pages=' . $arrQuery['page'] : '';
        $strParams .= '';
        
        $text = '';
        switch ($language) {
            case 'us':
                $text = 'my';
                break;
            case 'de':
                $text = 'duc';
                break;
            case 'jp':
                $text = 'nhat';
                break;
            case 'uk':
                $text = 'anh';
                break;
            default:
                break;
        }
        $strURL = '/tim-kiem-san-pham-amazon-' . $text . '/?' . $strParams;
        
        $strURL = str_replace('i:aps,', '', str_replace(' ', '+', $strURL));
        
        return $strURL;
    }
    
    /**
     *
     * @param type $arrData
     * @param type $limit
     */
    function generateGetLatestPriceMultipleProductKey($arrData = [], $limit)
    {
        $total = count($arrData);
        
        $arrAsinMerchant = array_chunk($arrData, $limit);
        $arrKeys         = [];
        foreach ($arrAsinMerchant as $arrData) {
            $strKey    = implode(',', array_filter($arrData));
            $arrKeys[] = openssl_encrypt($strKey, "AES-256-CBC", SECRET_KEY);
        }
        
        return $arrKeys;
    }
    
    /**
     * floatRating: số sao đánh giá sản phẩm
     */
    function rateMerchant($floatRating = 0, $intReview = 0)
    {
        if ($intReview > 50 && $floatRating >= 4.5) {
            return ['color' => 'bg-level-1', 'text' => 'Sản phẩm tốt, rất nhiều người đã mua'];
        }
        
        if ($intReview > 50 && $floatRating > 4) {
            return ['color' => 'bg-level-1', 'text' => 'Có phản hồi tốt - nên mua'];
        }
        
        if ($intReview > 5 && $floatRating <= 1) {
            return ['color' => 'bg-level-3', 'text' => 'Phản hồi tiêu cực - không nên mua'];
        }
        
        if ($intReview > 5 && $floatRating <= 1.5) {
            return ['color' => 'bg-level-3', 'text' => 'Phản hồi tiêu cực - không nên mua'];
        }
        
        if ($intReview > 5 && $floatRating <= 2) {
            return ['color' => 'bg-level-3', 'text' => 'Phản hồi không tốt - không nên mua'];
        }
        
        if ($intReview > 5 && $floatRating <= 2.5) {
            return ['color' => 'bg-level-3', 'text' => 'Phản hồi không tốt - không nên mua'];
        }
        
        if ($intReview > 5 && $floatRating <= 3) {
            return ['color' => 'bg-level-2', 'text' => 'Phản hồi bình thường - nên cân nhắc'];
        }
        
        if ($intReview > 5 && $floatRating <= 3.5) {
            return ['color' => 'bg-level-2', 'text' => 'Có phản hồi bình thường - nên cân nhắc'];
        }
        
        if ($intReview > 5 && $floatRating <= 4) {
            return ['color' => 'bg-level-1', 'text' => 'Tương đối tốt - rất nhiều người đã mua'];
        }
        
        if ($intReview > 5 && $floatRating >= 4.5) {
            return ['color' => 'bg-level-1', 'text' => 'Có phản hồi tốt - yên tâm mua'];
        }
        
        if ($intReview <= 5 && $floatRating <= 5) {
            return ['color' => 'bg-level-3', 'text' => 'Chưa có nhiều người mua - cẩn thận'];
        }
        
        return false;
    }
    
    
    function formatPrice($price, $roundPrecision = 2)
    {
        list($priceEven, $priceOdd) = explode('.', $price);
        if (empty($priceOdd)) {
            return $price . '.00';
        }
        
        return number_format($priceEven) . '.' . $priceOdd;
    }
    
    function parseRatingStarHtml($intRating)
    {
        if (!$intRating) {
            return '<i class="fas fa-star text--color-gray-light"></i><i class="fas fa-star text--color-gray-light"></i><i class="fas fa-star text--color-gray-light"></i><i class="fas fa-star text--color-gray-light"></i><i class="fas fa-star text--color-gray-light"></i>';
        }
        
        $strHtmlResult = '';
        $intRating     = round($intRating, 1);
        $fullStarScore = floor($intRating);
        $halfStarScore = $intRating - $fullStarScore;
        if ($fullStarScore > 0) {
            for ($i = 1; $i <= $fullStarScore; ++$i) {
                $strHtmlResult .= '<i class="fas fa-star text--color-yellow"></i>';
            }
        }
        if ($halfStarScore > 0) {
            switch ($halfStarScore) {
                case $halfStarScore <= 0.2:
                    $strHtmlResult .= '<i class="fas fa-star text--color-gray-light"></i>';
                    break;
                case $halfStarScore > 0.2 && $halfStarScore < 0.8:
                    $strHtmlResult .= '<i class="fas fa-star-half-alt text--color-yellow"></i>';
                    break;
                case $halfStarScore >= 0.8:
                    $strHtmlResult .= '<i class="fas fa-star text--color-yellow"></i>';
                    break;
            }
        }
        $totalStar = $halfStarScore > 0 ? $fullStarScore + 1 : $fullStarScore;
        if ($totalStar < 5) {
            for ($i = $totalStar; $i < 5; ++$i) {
                $strHtmlResult .= '<i class="fas fa-star text--color-gray-light"></i>';
            }
        }
        
        return $strHtmlResult;
    }
    
    function parseRatingStarMobileHtml($intRating)
    {
        if (!$intRating) {
            return '<i class="fa fa-star text-gray"></i><i class="fa fa-star text-gray"></i><i class="fa fa-star text-gray"></i><i class="fa fa-star text-gray"></i><i class="fa fa-star text-gray"></i>';
        }
        
        $strHtmlResult = '';
        $intRating     = round($intRating, 1);
        $fullStarScore = floor($intRating);
        $halfStarScore = $intRating - $fullStarScore;
        if ($fullStarScore > 0) {
            for ($i = 1; $i <= $fullStarScore; ++$i) {
                $strHtmlResult .= '<i class="fa fa-star text-yellow"></i>';
            }
        }
        if ($halfStarScore > 0) {
            switch ($halfStarScore) {
                case $halfStarScore <= 0.2:
                    $strHtmlResult .= '<i class="fa fa-star text-gray"></i>';
                    break;
                case $halfStarScore > 0.2 && $halfStarScore < 0.8:
                    $strHtmlResult .= '<i class="fa fa-star-half-alt text-yellow"></i>';
                    break;
                case $halfStarScore >= 0.8:
                    $strHtmlResult .= '<i class="fa fa-star text-yellow"></i>';
                    break;
            }
        }
        $totalStar = $halfStarScore > 0 ? $fullStarScore + 1 : $fullStarScore;
        if ($totalStar < 5) {
            for ($i = $totalStar; $i < 5; ++$i) {
                $strHtmlResult .= '<i class="fa fa-star text-gray"></i>';
            }
        }
        
        return $strHtmlResult;
    }
    
    function generateProductURL($uri, $query = '')
    {
    
    }
    
    function renderPhpFile($filename, $vars = null, &$arrReturn = [])
    {
        if (is_array($vars) && !empty($vars)) {
            extract($vars);
        }
        
        ob_start();
        include $filename;
        $arrReturn = [
            'arrGroupAsinMerchant'      => $arrGroupAsinMerchant,
            'strEncryptFinalPriceParam' => $strEncryptFinalPriceParam
        ];
        return ob_get_clean();
    }