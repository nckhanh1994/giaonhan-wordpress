<?php
    include_once(__DIR__ . '/BotAbstract.php');
    
    class Search extends BotAbstract
    {
        public function __construct()
        {
        
        }
        
        public function getResult()
        {
            switch ($this->action) {
                case 'get-list';
                    return $this->getList();
                default:
                    return [];
            }
        }
        
        public function getList()
        {
            $arrRequestParam = [
                'page'     => !empty($this->params['page']) ? $this->params['page'] : 1,
                'lang'     => !empty($this->params['lang']) ? $this->params['lang'] : 'us',
                'rh'       => !empty($this->params['rh']) ? $this->params['rh'] : '',
                'keywords' => !empty($this->params['keywords']) ? $this->params['keywords'] : '',
            ];
            
            $strParams = str_replace(' ', '+', 'rh=' . $arrRequestParam['rh'] . '&keywords=' . $arrRequestParam['keywords']);
            $strParams .= $arrRequestParam['page'] ? '&page=' . $arrRequestParam['page'] : '';
            $strParams .= $arrRequestParam['sort'] ? '&sort=' . $arrRequestParam['sort'] : '';
            
            $strURL = $this->setURL('/product-list.php?' . $strParams);
            $output = $this->crawler($strURL, [], $this->arrHeader);
            $output = json_decode($output, true);
            
            return $output;
        }
    }
