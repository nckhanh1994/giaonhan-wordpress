<?php
    require_once __DIR__ . '/Giaonhan.php';
    
    class Order extends Giaonhan
    {
        public function __construct()
        {
            parent::__construct();
        }
        
        public function getResult()
        {
            switch ($this->action) {
                case 'get_checkout_data':
                    return $this->getCheckoutData();
                case 'get_district_via_city_id':
                    return $this->getDistrictViaCityID();
                case 'get_ward_via_district_id':
                    return $this->getWardViaDistrictID();
                case 'checkout_order':
                    return $this->checkoutOrder();
                default:
                    break;
            }
        }
        
        /**
         * ===================================================================================
         * =============================== PRIVATE ===========================================
         * ===================================================================================
         */
        private function getCheckoutData()
        {
            $arrRequestParam = [
                'sessionID' => $this->params['sessionID']
            ];
            
            return $this->crawl('getCheckoutData', http_build_query($arrRequestParam), $this->arrHeader);
        }
        
        private function getDistrictViaCityID()
        {
            $arrRequestParam = [
                'sessionID' => $this->params['sessionID'],
                'id'        => !empty($this->params['city_id']) ? $this->params['city_id'] : 0
            ];
            
            return $this->crawl('getDistrictViaCityID', http_build_query($arrRequestParam), $this->arrHeader);
        }
        
        private function getWardViaDistrictID()
        {
            $arrRequestParam = [
                'sessionID' => $this->params['sessionID'],
                'id'        => !empty($this->params['district_id']) ? $this->params['district_id'] : 0
            ];
            
            return $this->crawl('getWardViaDistrictID', http_build_query($arrRequestParam), $this->arrHeader);
        }
        
        private function checkoutOrder()
        {
            $arrRequestParam = [
                'sessionID' => $this->params['sessionID'],
                'dataUser'  => [
                    'receiver_name'        => $this->params['receiver_name'],
                    'receiver_phone'       => $this->params['receiver_phone'],
                    'receiver_email'       => $this->params['receiver_email'],
                    'receiver_address'     => $this->params['receiver_address'],
                    'receiver_city_id'     => $this->params['receiver_city_id'],
                    'receiver_district_id' => $this->params['receiver_district_id'],
                    'receiver_ward_id'     => $this->params['receiver_ward_id'],
                ]
            ];
            
            return $this->crawl('checkoutOrder', http_build_query($arrRequestParam), $this->arrHeader);
        }
        
        
        private function crawl($action, $arrParam = [], $arrHeader = [])
        {
            $apiURL = $this->setURL($action);
            $output = $this->crawler($apiURL, $arrParam, $arrHeader);
            if ($this->debug) {
                echo '<pre>';
                print_r($output);
                echo '</pre>';
                die;
            }
            $output = json_decode($output, true);
            
            return $output;
        }
    }
