<?php
    
    
    class Giaonhan
    {
        const GIAONHAN_URL_DEV = 'http://dev.admin.giaonhan247.com/apis/giaonhan';
        const GIAONHAN_URL_STAGING = 'http://stagingv2.giaonhan247.com/apis/giaonhan';
        const GIAONHAN_URL_PRODUCTION = 'https://v2.giaonhan247.com/apis/giaonhan';
        
        protected $debug = 0;
        protected $url = null;
        protected $action = null;
        protected $params = null;
        protected $arrHeader = [
            'Content-Type: application/x-www-form-urlencoded',
            'Connection: Keep-Alive'
        ];
        
        /**
         * @construct
         */
        public function __construct()
        {
            switch (APPLICATION_ENV) {
                case 'production':
                    $this->url = self::GIAONHAN_URL_PRODUCTION;
                    break;
                case 'staging':
                    $this->url = self::GIAONHAN_URL_STAGING;
                    break;
                case 'dev':
                default:
                    $this->url = self::GIAONHAN_URL_DEV;
                    break;
                
            }
        }
        
        /**
         * @param string $url
         * @return string
         */
        public function setURL($url = '')
        {
            return $this->url . '/' . $url;
        }
        
        public function setDebug($debug)
        {
            $this->debug = $debug;
            
            return $this;
        }
        
        public function setAction($action)
        {
            $this->action = $action;
            
            return $this;
        }
        
        public function setParams($params)
        {
            $this->params = $params;
            
            return $this;
        }
        
        public static function crawler($strURL, $arrData = [], $arrHeaders = [], $strCookiePath = false)
        {
            $crawler = curl_init($strURL);
            if ($arrData) {
                curl_setopt($crawler, CURLOPT_POST, 1);
                curl_setopt($crawler, CURLOPT_POSTFIELDS, $arrData);
            }
            if ($arrHeaders) {
                curl_setopt($crawler, CURLOPT_HTTPHEADER, $arrHeaders);
            }
            if ($strCookiePath) {
                curl_setopt($crawler, CURLOPT_COOKIEJAR, $strCookiePath);
                curl_setopt($crawler, CURLOPT_COOKIEFILE, $strCookiePath);
                curl_setopt($crawler, CURLOPT_COOKIE, session_name() . '=' . session_id());
            }
            curl_setopt($crawler, CURLOPT_TIMEOUT, 30);
            curl_setopt($crawler, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($crawler, CURLOPT_COOKIESESSION, true);
            curl_setopt($crawler, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($crawler, CURLOPT_ENCODING, '');
            curl_setopt($crawler, CURLOPT_DNS_CACHE_TIMEOUT, 0);
            curl_setopt($crawler, CURLOPT_MAXREDIRS, 5);
            curl_setopt($crawler, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($crawler, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($crawler);
            curl_close($crawler);
            
            return $data;
        }
    }
