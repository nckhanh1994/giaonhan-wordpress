<?php
    require_once __DIR__ . '/Giaonhan.php';
    
    class Cart extends Giaonhan
    {
        public function __construct()
        {
            parent::__construct();
        }
        
        public function getResult()
        {
            switch ($this->action) {
                case 'get_cart_data':
                    return $this->getCartData();
                case 'add_to_cart':
                    return $this->addToCart();
                case 'update_cart_item':
                    return $this->updateCartItem();
                case 'remove_cart_item':
                    return $this->removeCartItem();
                case 'change_product_type':
                    return $this->changeProductType();
                default:
                    break;
            }
        }
        
        /**
         * ===================================================================================
         * =============================== PRIVATE ===========================================
         * ===================================================================================
         */
        private function getCartData()
        {
            $arrRequestParam = [
                'sessionID' => $this->params['sessionID']
            ];
            
            return $this->crawl('getCartData', http_build_query($arrRequestParam), $this->arrHeader);
        }
        
        private function addToCart()
        {
            $arrRequestParam = [
                'type'               => $this->params['type'],
                'sessionID'          => $this->params['sessionID'],
                'itemLink'           => !empty($this->params['itemLink']) ? $this->params['itemLink'] : '',
                'itemLang'           => !empty($this->params['itemLang']) ? $this->params['itemLang'] : '',
                'itemQuantity'       => !empty($this->params['itemQuantity']) ? $this->params['itemQuantity'] : 1,
                'strEncryptCartData' => !empty($this->params['strEncryptCartData']) ? $this->params['strEncryptCartData'] : ''
            ];
            
            return $this->crawl('addToCart', http_build_query($arrRequestParam), $this->arrHeader);
        }
        
        private function updateCartItem()
        {
            $arrRequestParam = [
                'sessionID' => $this->params['sessionID'],
                'id'        => $this->params['id'],
                'quantity'  => $this->params['quantity']
            ];
            
            return $this->crawl('updateCartItem', http_build_query($arrRequestParam), $this->arrHeader);
        }
        
        private function removeCartItem()
        {
            $arrRequestParam = [
                'sessionID' => $this->params['sessionID'],
                'id'        => $this->params['id']
            ];
            
            return $this->crawl('removeCartItem', http_build_query($arrRequestParam), $this->arrHeader);
        }
        
        private function changeProductType()
        {
            $arrRequestParam = [
                'sessionID'     => $this->params['sessionID'],
                'id'            => $this->params['id'],
                'productTypeID' => $this->params['productTypeID']
            ];
            
            return $this->crawl('changeProductType', http_build_query($arrRequestParam), $this->arrHeader);
        }
        
        private function crawl($action, $arrParam = [], $arrHeader = [])
        {
            $apiURL = $this->setURL($action);
            $output = $this->crawler($apiURL, $arrParam, $arrHeader);
            if ($this->debug) {
                echo '<pre>';
                print_r($output);
                echo '</pre>';
                die;
            }
            $output = json_decode($output, true);
            
            return $output;
        }
    }
