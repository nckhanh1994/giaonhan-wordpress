<?php
    require_once __DIR__ . '/MzPage.php';
    require_once __DIR__ . '/iMzPage.php';
    require_once MZ_PLUGIN_PATH . '/includes/Fado/Utils.php';
    
    class MzViewCartPage extends MzPage implements iMzPage
    {
        private $pageTemplate = MZ_VIEW_CART_PAGE;
        
        public function __construct()
        {
        
        }
        
        public function addHook()
        {
            add_action('mz_enqueue_style', [&$this, 'enqueueStyle']);
            add_action('mz_enqueue_script', [&$this, 'enqueueScript']);
            
            return $this;
        }
        
        public function doHook()
        {
            do_action('mz_enqueue_style');
            do_action('mz_enqueue_script');
            
            return $this;
        }
        
        public function enqueueStyle()
        {
            $arrFile = [
                'vendor-style.css', 'cart-page-style.css'
            ];
            
            $dirPath = MZ_PLUGIN_URI . '/assets/css';
            foreach ($arrFile as $fileName)
                wp_enqueue_style($fileName, $dirPath . '/' . $fileName);
        }
        
        public function enqueueScript()
        {
            /**
             * ===================================================================================
             * =============================== LIBS ==============================================
             * ===================================================================================
             */
            $arrFile = [
                'lazyload.min.js'
            ];
            
            $dirPath = MZ_PLUGIN_URI . '/assets/js/libs';
            foreach ($arrFile as $fileName)
                wp_enqueue_script($fileName, $dirPath . '/' . $fileName, ['jquery']);
            
            /**
             * ===================================================================================
             * =============================== MY ================================================
             * ===================================================================================
             */
            wp_enqueue_script($this->pageTemplate, MZ_PLUGIN_URI . '/assets/js/my/mz-view-cart-page.js', ['jquery']);
            wp_localize_script($this->pageTemplate, 'ajaxOption', $arrParam);
        }
    }

?>