<?php
    require_once __DIR__ . '/MzPage.php';
    require_once __DIR__ . '/iMzPage.php';
    
    class MzSearchAllCountryPage extends MzPage implements iMzPage
    {
        private $pageTemplate = MZ_SEARCH_ALL_COUNTRY_PAGE;
        private $keyword = '';
        
        public function __construct()
        {
        }
        
        public function setKeyword($keyword)
        {
            $this->keyword = $keyword;
            
            return $this;
        }
        
        public function addHook()
        {
            add_action('mz_enqueue_style', [&$this, 'enqueueStyle']);
            add_action('mz_enqueue_script', [&$this, 'enqueueScript']);
            
            add_filter('mz_filter_keyword', [&$this, 'filterKeyword']);
            add_filter('mz_generate_category_uri', [&$this, 'generateCategoryURI']);
            
            return $this;
        }
        
        public function doHook()
        {
            do_action('mz_enqueue_style');
            do_action('mz_enqueue_script');
            
            return $this;
        }
        
        public function enqueueStyle()
        {
            $arrFile = [
                'vendor-style.css', 'font-awesome.css', 'search-result-page-style.css', 'swiper.min.css'
            ];
            
            $dirPath = MZ_PLUGIN_URI . '/assets/css';
            foreach ($arrFile as $fileName)
                wp_enqueue_style($fileName, $dirPath . '/' . $fileName);
        }
        
        public function enqueueScript()
        {
            /**
             * ===================================================================================
             * =============================== LIBS ==============================================
             * ===================================================================================
             */
            $arrFile = [
            
            ];
            
            $dirPath = MZ_PLUGIN_URI . '/assets/js/libs';
            foreach ($arrFile as $fileName)
                wp_enqueue_script($fileName, $dirPath . '/' . $fileName);
            
            
            /**
             * ===================================================================================
             * =============================== MY ================================================
             * ===================================================================================
             */
            
            $arrParam = [
                'ajaxURL'      => admin_url('admin-ajax.php'),
                'arrLang'      => ['us', 'jp', 'de', 'uk'],
                'requestParam' => openssl_encrypt(json_encode([
                    'intPage'  => $this->page,
                    'keywords' => $this->keyword,
                    'rh'       => $this->keyword,
                    'sort'     => '',
                ]), "AES-256-CBC", SECRET_KEY),
            ];
            
            wp_enqueue_script($this->pageTemplate, MZ_PLUGIN_URI . '/assets/js/my/' . $this->pageTemplate . '.js', ['jquery']);
            wp_localize_script($this->pageTemplate, 'ajaxOption', $arrParam);
        }
        
        public function filterKeyword()
        {
            $params = $_GET;
            
            $keywords = !empty($params['keywords']) ? trim($params['keywords']) : '';
            
            return $keywords;
        }
        
        public function generateCategoryURI($lang)
        {
            global $arrCountryCode;
            
            $params = $_GET;
            
            $keywords = !empty($params['keywords']) ? trim($params['keywords']) : '';
            
            $strQuery = str_replace(' ', '+', 'rh=' . $keywords . '&keywords=' . $keywords);
            $strQuery .= '';
            
            return get_site_url() . '/' . MZ_SEARCH_COUNTRY_ROUTE_BEGIN . '-' . $arrCountryCode[ $lang ]['slug'] . '?' . $strQuery;
        }
    }

?>