<?php
    require_once __DIR__ . '/MzPage.php';
    require_once __DIR__ . '/iMzPage.php';
    require_once MZ_PLUGIN_PATH . '/includes/Fado/Utils.php';
    
    class MzSearchCountryPage extends MzPage implements iMzPage
    {
        private $pageTemplate = MZ_SEARCH_COUNTRY_PAGE;
        
        private $lang = '';
        private $keyword = '';
        
        public function __construct()
        {
        
        }
        
        public function setLang($lang)
        {
            $this->lang = $lang;
            
            return $this;
        }
        
        public function setKeyword($keyword)
        {
            $this->keyword = $keyword;
            
            return $this;
        }
        
        public function addHook()
        {
            add_action('mz_enqueue_style', [&$this, 'enqueueStyle']);
            add_action('mz_enqueue_script', [&$this, 'enqueueScript']);
            add_action('mz_get_product_list', [&$this, 'getProductList']);
            
            return $this;
        }
        
        public function doHook()
        {
            do_action('mz_enqueue_style');
            do_action('mz_enqueue_script');
            
            return $this;
        }
        
        public function enqueueStyle()
        {
            $arrFile = [
                'vendor-style.css', 'category-page-style.css'
            ];
            
            $dirPath = MZ_PLUGIN_URI . '/assets/css';
            foreach ($arrFile as $fileName)
                wp_enqueue_style($fileName, $dirPath . '/' . $fileName);
        }
        
        public function enqueueScript()
        {
            /**
             * ===================================================================================
             * =============================== LIBS ==============================================
             * ===================================================================================
             */
            $arrFile = [
                'lazyload.min.js'
            ];
            
            $dirPath = MZ_PLUGIN_URI . '/assets/js/libs';
            foreach ($arrFile as $fileName)
                wp_enqueue_script($fileName, $dirPath . '/' . $fileName, ['jquery']);
            
            /**
             * ===================================================================================
             * =============================== MY ================================================
             * ===================================================================================
             */
            
        }
        
        public function getProductList()
        {
            global $arrCountryCode;
            
            $page        = $this->page;
            $lang        = $this->lang;
            $strKeywords = $this->keyword;
            
            $arrRequestParam = [
                'page'     => $page,
                'lang'     => $lang,
                'keywords' => $strKeywords,
                'rh'       => $strKeywords,
                'sort'     => ''
            ];
            
            $arrFadoData = (new Search())
                ->setLang($lang)
                ->setParams($arrRequestParam)
                ->setAction('get-list')
                ->getResult();
            
            $viewPath = MZ_PLUGIN_PATH . '/views/desktop/partial/category-block.php';
            $html     = renderPhpFile($viewPath, ['page' => $page, 'lang' => $lang, 'arrFadoData' => $arrFadoData, 'strKeywords' => $strKeywords, 'countryTxt' => $arrCountryCode[ $lang ]['txt']], $arrReturn);
            
            $arrParam = [
                'ajaxURL'              => admin_url('admin-ajax.php'),
                'lang'                 => $lang,
                'arrGroupAsinMerchant' => $arrReturn['arrGroupAsinMerchant']
            ];
            
            wp_enqueue_script($this->pageTemplate, MZ_PLUGIN_URI . '/assets/js/my/' . $this->pageTemplate . '.js', ['jquery']);
            wp_localize_script($this->pageTemplate, 'ajaxOption', $arrParam);
            
            echo $html;
        }
    }

?>