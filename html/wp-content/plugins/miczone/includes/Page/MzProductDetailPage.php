<?php
    require_once __DIR__ . '/MzPage.php';
    require_once __DIR__ . '/iMzPage.php';
    require_once MZ_PLUGIN_PATH . '/includes/Fado/Product.php';
    
    class MzProductDetailPage extends MzPage implements iMzPage
    {
        private $pageTemplate = MZ_PRODUCT_DETAIL_PAGE;
        
        private $lang = null;
        private $asin = null;
        private $merchantID = null;
        private $keyword = null;
        
        public function __construct()
        {
        
        }
        
        public function setLang($lang)
        {
            $this->lang = $lang;
            
            return $this;
        }
        
        public function setAsin($asin)
        {
            $this->asin = $asin;
            
            return $this;
        }
        
        public function setMerchantID($merchantID)
        {
            $this->merchantID = $merchantID;
            
            return $this;
        }
        
        public function setKeyword($keyword)
        {
            $this->keyword = $keyword;
            
            return $this;
        }
        
        public function addHook()
        {
            add_action('mz_enqueue_style', [&$this, 'enqueueStyle']);
            add_action('mz_enqueue_script', [&$this, 'enqueueScript']);
            add_action('mz_render_product_detail', [&$this, 'renderProductDetail']);
            
            return $this;
        }
        
        public function doHook()
        {
            do_action('mz_enqueue_style');
            do_action('mz_enqueue_script');
            
            return $this;
        }
        
        public function enqueueStyle()
        {
            $arrFile = [
                'vendor-style.css', 'font-awesome.css', 'product-detail-page-style.css', 'swiper.min.css'
            ];
            
            $dirPath = MZ_PLUGIN_URI . '/assets/css';
            foreach ($arrFile as $fileName)
                wp_enqueue_style($fileName, $dirPath . '/' . $fileName);
        }
        
        public function enqueueScript()
        {
            /**
             * ===================================================================================
             * =============================== LIBS ==============================================
             * ===================================================================================
             */
            $arrFile = [
                'swiper.min.js', 'lazyload.min.js'
            ];
            
            $dirPath = MZ_PLUGIN_URI . '/assets/js/libs';
            foreach ($arrFile as $fileName)
                wp_enqueue_script($fileName, $dirPath . '/' . $fileName, ['jquery']);
            
            
            /**
             * ===================================================================================
             * =============================== MY ================================================
             * ===================================================================================
             */
        }
        
        public function renderProductDetail()
        {
            $lang       = $this->lang;
            $asin       = $this->asin;
            $merchantID = $this->merchantID;
            
            $arrRequestParam = [
                'lang'       => $lang,
                'asin'       => $asin,
                'merchantID' => $merchantID
            ];
            
            $arrProduct = (new Product())
                ->setLang($lang)
                ->setParams($arrRequestParam)
                ->setAction('get-detail')
                ->getResult();
            
            $viewPath = MZ_PLUGIN_PATH . '/views/desktop/product-detail-block.php';
            $html     = renderPhpFile($viewPath, ['lang' => $lang, 'arrProduct' => $arrProduct], $arrReturn);
            
            $arrParam = [
                'ajaxURL'                   => admin_url('admin-ajax.php'),
                'lang'                      => $lang,
                'itemLink'                  => BASE_URL . $_SERVER['REQUEST_URI'],
                'strEncryptFinalPriceParam' => $arrReturn['strEncryptFinalPriceParam']
            ];
            
            wp_enqueue_script($this->pageTemplate, MZ_PLUGIN_URI . '/assets/js/my/' . $this->pageTemplate . '.js', ['jquery']);
            wp_localize_script($this->pageTemplate, 'ajaxOption', $arrParam);
            
            echo $html;
        }
    }

?>