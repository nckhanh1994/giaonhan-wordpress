<?php
    include __DIR__ . '/iMzPage.php';
    
    class MzPage
    {
        private $_instance = null;
        protected $page = 1;
        
        public function __construct(iMzPage $_iMzPage)
        {
            $this->_instance = $_iMzPage;
        }
        
        public function setPage($page = 1)
        {
            $this->page = $page;
            
            return $this;
        }
        
        public function run()
        {
            $this->_instance
                ->addHook()
                ->doHook();
            
            return $this;
        }
    }

?>