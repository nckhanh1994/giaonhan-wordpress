<?php
    $key                       = $arrProduct['asin'] . '-' . $arrProduct['quoteMerchant']['merchantID'] . '-new';
    $strEncryptFinalPriceParam = openssl_encrypt(json_encode([
        $key => [
            'lang'            => $lang,
            'isEnterprise'    => 0,
            'isInventory'     => 0,
            'productName'     => $arrProduct['productName'],
            'productImage'    => current($arrProduct['productImage']),
            'productLink'     => $_SERVER['REQUEST_URI'],
            'asin'            => $arrProduct['asin'],
            'currentMerchant' => $arrProduct['quoteMerchant']['merchantID'],
            'shippingWeight'  => $arrProduct['shippingWeight'],
            'categoryInfo'    => $arrProduct['categoryInfo'],
            'quoteMerchant'   => $arrProduct['quoteMerchant'],
        ],
    ]), "AES-256-CBC", SECRET_KEY);
    
    // Mô tả sản phẩm
    $strProductDescription = '';
    foreach ($arrProduct['productInfo']['productFeatures'] as $k => $value) {
        $strProductDescription .= '<li>' . $value . '</li>';
    }
    
    $countryCode = '';
    $currency    = '$';
    switch ($lang) {
        case 'us':
            $countryCode = 'Mỹ';
            break;
        case 'jp':
            $countryCode = 'Nhật';
            $currency    = '¥‎';
            break;
        case 'de':
            $countryCode = 'Đức';
            $currency    = '€';
        case 'uk':
            $countryCode = 'Anh';
            $currency    = '£';
            break;
            break;
    }
    
    #region - Format description list
    $arrDescList = [];
    if (count($arrProduct['productInfo']['productFeatures']) > 0) {
        $arrDescList = $arrProduct['productInfo']['productFeatures'];
    }
    
    if (count($arrProduct['productInfo']['productDescription']) > 0) {
        $arrDescList = array_merge($arrDescList, [$arrProduct['productInfo']['productDescription']]);
    }
    
    $arrDescList = array_filter($arrDescList);
    #endregion
?>
<div class="container pd-layout__container" style="border: 1px solid #eee">
    <div class="pd-layout__head">
        <div class="pd-layout__product-info-segment">
            <h1 class="pd-layout__product-title" id="productName"><?php echo $arrProduct['productName'] ?></h1>
            <div class="pd-layout__meta-item-wrap">
                <div class="pd-layout__meta-item">
                    <?php echo parseRatingStarHtml($arrProduct['productTotalStar']); ?> <?php echo (int)$arrProduct['productTotalStar']; ?>/5
                    
                    <?php if ($arrProduct['productTotalReview']): ?>
                        (<?php echo $arrProduct['productTotalReview']; ?> lượt đánh giá)
                    <?php endif; ?>
                </div>

                <div class="pd-layout__meta-item">
                    <i class="fal fa-comment-lines"></i>
                    <span class="font--weight-500">Gợi ý:</span>
                    <span class="text--color-link">
                                                   <?php if ($merchantName == 'Amazon' || $merchantName == 'Amazon.com') : ?>
                                                       An tâm mua sắm.
                                                   <?php elseif (!empty($rateMerchant = rateMerchant($arrProduct['productTotalStar'], $arrProduct['productTotalReview']))): ?>
                                                       <?php echo ucfirst($rateMerchant['text']) ?>
                                                   <?php endif; ?>
                                            </span>
                </div>

                <div class="pd-layout__meta-item">
                    Bán tại:
                    <i class="svg -svg-logo-amz-smile -svg-24px"></i>
                    Amazon <?php echo $countryCode ?>
                </div>

                <div class="pd-layout__meta-item">
                    Thương hiệu:
                    <a href="#" target="_blank"><?php echo $arrProduct['brand']['name'] ?></a>
                </div>

            </div>
        </div>

        <div class="pd-layout__shop-segment">
            <div class="pd-layout__shop-segment__inner">
                <i class="svg -svg-shop -svg-32px pd-layout__shop-segment-icon"></i>

                <div class="pd-layout__shop-segment__info-col">
                    <div class="pd-layout__shop-title">
                        Người bán:
                        <a class="text--color-blue" href="#"><?php echo $arrProduct['quoteMerchant']['merchantName'] ?></a>
                    </div>

                    <div class="pd-layout__shop-meta">
                        <i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i> | 100% đánh giá uy tín
                    </div>
                </div>
            </div>
        </div><!-- .pd-layout__shop-segment -->
    </div>
    
    <?php
        $arrImageList = array_filter($arrProduct['productImage'], function ($value) {
            return parse_url($value, PHP_URL_HOST);
        });
        
        $strNullLargeImageBlock = "
                  <div class='swiper-slide'>
                    <a class='pd-image-block__gallery-item' data-fancybox='pd-image' href='' target='_blank'>
                      <img class='lazyload pd-image-block__gallery-img' src='' data-src='' alt=''/>
                    </a>
                  </div>
                ";
        
        $i = 0;
        //Handle image block slider
        if ($arrImageList) {
            $thumbImage = null;
            $largeImage = null;
            foreach ($arrImageList as $key => $strImage) {
                if ($strImage) {
                    $thumbImage = str_replace('.jpg', '.SR80,80_.jpg', $strImage);
                    $largeImage = preg_replace('/._S(X|L|Y|S)[0-9]+_(?!.*cac)/mi', '', $strImage);
                    
                    $strIsActive = '';
                    if ($i == 0) {
                        $strImageJs  = $strImage;
                        $strIsActive = 'is-active';
                    }
                    
                    $strLargeImageBlock .= "
                                <div class='swiper-slide'>
                                    <a class='pd-image-block__gallery-item' data-fancybox='pd-image' href='$largeImage' target='_blank'>
                                      <img class='lazyload pd-image-block__gallery-img' src='' data-src='$largeImage' alt='' data-expand='4000'/>
                                    </a>
                                </div>";
                    $strThumbImageBlock .= "
                                <div class='swiper-slide'>
                                    <div class='pd-image-block__thumb-gallery-item $strIsActive' data-target='$i'>
                                      <img class='lazyload pd-image-block__thumb-gallery-img' src='' data-src='$thumbImage' alt=''/>
                                    </div>
                                </div>";
                    $i++;
                } else {
                    $strLargeImageBlock .= $strNullLargeImageBlock;
                    $strThumbImageBlock .= "
                            <div class='swiper-slide'>
                                <div class='pd-image-block__thumb-gallery-item' data-target='$i'>
                                  <img class='lazyload pd-image-block__thumb-gallery-img' src='' data-src='' alt=''/>
                                </div>
                            </div>";
                }
            }
        }
        
        //Handle video block slider
        if ($arrVideoList) {
            foreach ($arrVideoList as $k => $arrVideo) {
                $strLargeVideoBlock .= "
                        <div class='swiper-slide'>
                          <div class='pd-image-block__gallery-img'>
                            <video class='pd-image-block__gallery-video' poster='" . $arrVideo['thumb'] . "' playsinline controls>
                              <source src='" . $arrVideo['videoURL'] . "' type='video/mp4'/>
                            </video>
                          </div>
                        </div>";
                $strThumbVideoBlock .= "
                        <div class='swiper-slide'>
                            <div class='pd-image-block__thumb-gallery-item' data-target='" . $i++ . "'>
                              <img class='lazyload pd-image-block__thumb-gallery-img' src='' data-src='" . $arrVideo['thumb'] . "' alt=''/>
                            </div>
                        </div>";
            }
        }
        
        //If product does not have any images or videos, show this
        if (!$arrImageList && !$arrVideoList) {
            $strLargeImageBlock .= $strNullLargeImageBlock;
            $strThumbImageBlock .= "
                    <div class='swiper-slide'>
                        <div class='pd-image-block__thumb-gallery-item' data-target='$i'>
                          <img class='lazyload pd-image-block__thumb-gallery-img' src='' data-src='' alt=''/>
                        </div>
                     </div>";
        }
    ?>
    <div class="pd-layout__sidebar-col" style="position: relative;">
        <section id="pd-image-block" class="pd-image-block">
            <?php if ($arrProduct['quoteMerchant']['percentageDiscount']) : ?>
                <div class="pd-image-block__sale-tag">-<?php echo round($arrProduct['quoteMerchant']['percentageDiscount']) ?>%
                </div>
            <?php endif; ?>

            <div id="pd-image-block__hover-image-field" class="pd-image-block__hover-image-field">
                <img id="pd-image-block__hover-img" class="pd-image-block__hover-img" src="" alt=""/>
            </div>

            <div id="pd-image-block__swiper-container" class="swiper-container pd-image-block__swiper-container">
                <div class="swiper-wrapper">
                    <?php echo $strLargeImageBlock ?>
                    <?php echo $strLargeVideoBlock ?>
                </div>
            </div>

            <div class="pd-image-block__thumb-segment">
                <div id="pd-image-block__thumb-swiper-container" class="swiper-container pd-image-block__thumb-swiper-container">
                    <div class="swiper-wrapper">
                        <?php echo $strThumbImageBlock ?>
                        <?php echo $strThumbVideoBlock ?>
                    </div>
                </div>

                <div id="pd-image-block__next-btn" class="pd-image-block__next-btn"></div>
                <div id="pd-image-block__prev-btn" class="pd-image-block__prev-btn"></div>
            </div>
        </section>
    </div>

    <div class="pd-layout__main-col">
        <section class="pd-block product-detail-block">
            <div id="pd-block__main-col" class="pd-block__main-col">
                <div class="pd-block__change-price-segment">
                    <a class="pd-block__change-price-btn js-ttip is-active" data-ttip-title="Dành cho người mua cá nhân.<br/>Giá đã bao gồm tất cả các loại thuế phí" href="javascript:;" data-ttip="true">
                        Giá trọn gói về Việt Nam
                    </a>
                </div>

                <div class="price-box js-price-box" id="html-tax-data">
                    <div class="box-head">
                        <div class="pd-block__price-info-item">
                            <div class="pd-block__price-info-item__lbl" style="font-size: 20px; margin-right: 10px;">
                                Giá:
                            </div>

                            <div class="pd-block__price-info-item__val">
                                <p class="main-price" style="display: inline-block">
                                    $<span class="js-amazon__final-price">&nbsp;<i class="fa fa-spinner fa-spin" aria-hidden="true"></i></span>
                                </p>
                                <div class="pd-block__quantity-control" style="display: inline-block; margin-left: 30px;">
                                    <input type="number" class="js-amazon-quantity pd-block__quantity-input quantity-input" value="1" min="1">
                                    <div class="pd-block__quantity-control-btn -plus-btn">+</div>
                                    <div class="pd-block__quantity-control-btn -minus-btn is-disabled">-</div>
                                </div>
                                <button class="js-amazon__add-to-cart my-btn -btn-block -btn-grd-border -btn-pill  pd-block__add-cart-btn add-to-cart-btn" style="width: auto; margin-left: 15px">
                                    <div class="my-btn -btn-block -btn-pill -btn-grd-bg ">Thêm vào giỏ hàng&nbsp; <i class="fal fa-cart-plus margin--left-5px"></i>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div><!-- .box-head -->
                    <div class="box-main" style="display: block; padding: 15px 20px;">
                        <table class="price-tb">
                            <tbody>
                                <tr class="js-amazon__block-final-price">
                                    <td class="lbl">
                                        <span data-toggle="tooltip" data-original-title="Giá trọn gói về Việt Nam ">Giá trọn gói về Việt Nam</span>
                                    </td>
                                    <td class="val notranslate">$<span class="js-amazon__value">...</span>
                                    </td>
                                </tr>

                                <tr class="js-amazon__block-unit-price">
                                    <td class="lbl">
                                        <span data-toggle="tooltip" data-original-title="Giá sản phẩm ">Giá sản phẩm</span>
                                    </td>
                                    <td class="val notranslate">$<span class="js-amazon__value">...</span>
                                    </td>
                                </tr>

                                <tr class="js-amazon__block-local-tax">
                                    <td class="lbl">
                                        <span data-toggle="tooltip" data-original-title="Thuế nội địa ">Thuế nội địa</span>
                                    </td>
                                    <td class="val notranslate">$<span class="js-amazon__value">...</span>
                                    </td>
                                </tr>

                                <tr class="js-amazon__block-shipping-weight">
                                    <td class="lbl">
                                        <span data-toggle="tooltip" title="" data-original-title="Trọng lượng">Trọng lượng</span>
                                    </td>
                                    <td class="val notranslate">
                                        $<span class="js-amazon__value">...</span>
                                    </td>
                                </tr>

                                <tr class="js-amazon__block-shipping-fee">
                                    <td class="lbl">
                                        <span data-toggle="tooltip" title="" data-original-title="Phí vận chuyển nội địa">Phí vận chuyển nội địa</span>
                                    </td>
                                    <td class="val notranslate">
                                        $<span class="js-amazon__value">...</span>
                                    </td>
                                </tr>

                                <tr class="js-amazon__block-import-fee">
                                    <td class="lbl">Phí thông quan</td>
                                    <td class="val notranslate">
                                        $<span class="js-amazon__value">...</span>
                                    </td>
                                </tr>

                                <tr class="js-amazon__block-other-charge-fee">
                                    <td class="lbl">
                                        <span data-toggle="tooltip" title="" data-original-title="Phụ phí">Phụ phí</span>
                                    </td>
                                    <td class="val notranslate">
                                        $<span class="js-amazon__value">...</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table><!-- .price-tb -->
                    </div><!-- .box-main -->
                </div>
            </div><!-- .pd-block__main-col -->


        </section>
        
        <?php
            if (!empty($arrProduct['alsoBought'])):
                ?>
                <section class="pd-tab-block">
                    <div class="pd-tab-block__block-head">
                        <div class="pd-tab-block__block-head__title-col">Sản phẩm được mua cùng</div>
                    </div>

                    <div class="pd-tab-block__block-body">
                        <div class="pd-tab-block__also-bought-product-item-wrap">
                            <?php
                                $arrProduct['alsoBought'] = array_slice($arrProduct['alsoBought'], 0, 5);
                                foreach ($arrProduct['alsoBought'] as $intIdx => $arrProductItem):
                                    $arrProduct['productUrl'] = '/tim-kiem-san-pham.html?rh=k:' . $arrProductItem['asin'] . '&keywords=' . $arrProductItem['asin'];
                                    ?>
                                    <div
                                            class="pd-tab-block__also-bought-product-item"
                                            data-product_id="<?php echo $arrProductItem['asin']; ?>"
                                            data-product_name="<?php echo $arrProductItem['title']; ?>"
                                            data-product_list="<?php echo $strBlockTitle; ?>"
                                            data-product_position="<?php echo $intIdx + 1; ?>"
                                    >
                                        <a class="pd-tab-block__also-bought-product-item__img-field" href="<?php echo $arrProduct['productUrl']; ?>" target="_blank">
                                            <div class="pd-tab-block__also-bought-product-item__img-field-inner">
                                                <img
                                                        class="lazyload pd-tab-block__also-bought-product-item__product-img"
                                                        src="<?php echo MZ_PLUGIN_PATH ?>/assets/images/iconsloading-globe-gray.png"
                                                        data-src="<?php echo $arrProductItem['image']; ?>"
                                                        alt=""
                                                />
                                            </div>
                                        </a>

                                        <div class="pd-tab-block__also-bought-product-item__title">
                                            <a href="<?php echo $arrProductItem['productUrl']; ?>" target="_blank"><?php echo $arrProductItem['title']; ?></a>
                                        </div>
                                    </div>
                                <?php
                                endforeach;
                            ?>
                        </div><!-- .pd-tab-block__also-bought-product-item-wrap -->
                    </div><!-- .pd-tab-block__block-body -->
                </section><!-- .pd-tab-block -->
            <?php
            endif;
        ?>

        <section id="mo-ta" class="pd-tab-block">
            <div class="pd-tab-block__block-head">
                <div class="pd-tab-block__block-head__title-col">Mô tả</div>
            </div>

            <div class="pd-tab-block__block-body">
                <ul class="pd-tab-block__desc-list">
                    <?php echo $strProductDescription ?>
                </ul>
            </div>
        </section>

        <section id="thong-so" class="pd-tab-block">
            <div class="pd-tab-block__block-head">
                <div class="pd-tab-block__block-head__title-col">Thông số</div>
            </div>

            <div class="pd-tab-block__block-body">
                <ul class="pd-tab-block__param-list">
                    <?php foreach ($arrProduct['productInfo']['productDetails'] as $strTitle => $strProductInfo): ?>
                        <li class="pd-tab-block__param-item">
                            <div class="pd-tab-block__param-item__lbl-col"><?php echo $strTitle ?></div>
                            <div class="pd-tab-block__param-item__info-col"><?php echo $strProductInfo ?></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </section>
        
        <?php if (!empty($arrProduct['productInfo']['fromTheManufacturer'])): ?>
            <section id="thong-tin" class="pd-tab-block pd-tab-block--desc-content">
                <div class="pd-tab-block__block-head">
                    <div class="pd-tab-block__block-head__title-col">Thông tin</div>
                </div>

                <div class="pd-tab-block__block-body">
                    <div class="pd-tab-block__desc-content-segment">
                        <?php echo $arrProduct['productInfo']['fromTheManufacturer']; ?>
                    </div>
                </div>
            </section>
        <?php endif; ?>
        
        <?php
            // Hiển thị hình ảnh
            if (count($arrProduct['productImage']) >= 1):
                $arrImageList = array_filter($arrProduct['productImage'], function ($value) {
                    return parse_url($value, PHP_URL_HOST);
                });
                ?>
                <section class="pd-tab-block pd-tab-block--gallery">
                    <div class="pd-tab-block__block-head">
                        <div class="pd-tab-block__block-head__title-col">Hình ảnh sản phẩm</div>
                    </div>

                    <div class="pd-tab-block__block-body">
                        <div class="pd-tab-block__gallery-item-wrap">
                            <?php
                                $strThumbImageUrl         = '';
                                foreach ($arrImageList as $k => $strImageUrl):
                                    if ($strImageUrl):
                                        $strImageUrl = preg_replace('/._S(X|L|Y|S)[0-9]+_(?!.*cac)/mi', '', $strImageUrl);
                                        $strThumbImageUrl = str_replace('.jpg', '.SR200,200_.jpg', $strImageUrl);
                                        ?>
                                        <div class="pd-tab-block__gallery-item">
                                            <img
                                                    class="lazyload"
                                                    src=""
                                                    data-src="<?php echo $strImageUrl; ?>"
                                                    alt=""
                                            />
                                        </div>
                                    <?php
                                    endif;
                                endforeach;
                            ?>
                        </div>
                    </div>
                </section>
            <?php
            endif
        ?>
    </div>
</div>