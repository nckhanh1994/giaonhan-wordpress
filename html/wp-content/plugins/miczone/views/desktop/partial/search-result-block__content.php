<script type="text/javascript" src="<?php echo MZ_PLUGIN_URI . '/assets/js/libs/swiper.min.js' ?>"></script>
<?php
    $arrAsinMerchant = [];
    foreach ($arrFadoData['blockRight'] as $product):
        $arrMerchantInfo = $product['merchant'];
        $arrLinkInfo = $product['link'];
        
        $productURL = $arrLinkInfo['url'] . '&smid=' . $arrMerchantInfo['id'] . '&lang=' . $lang;
        
        $arrAsinMerchant[] = $product['asin'] . '-' . $arrMerchantInfo['id']; // Asin - MerchantID
        ?>
        <div class="swiper-slide" style="width: 224px; margin-right: 20px;">
            <div
                    class="product-box product-panel js-product-block is-loading-price"
                    data-country-code="us"
                    data-get-final-price=""
                    data-asin="<?php echo $product['asin'] ?>"
                    data-merchant-id="<?php echo $arrMerchantInfo['merchant'] ?>"
                    data-product-title="<?php echo $product['title'] ?>"
                    data-product_id="<?php echo $product['asin'] . '-' . $arrMerchantInfo['id'] ?>"
                    data-product_name="<?php echo $product['title'] ?>"
                    data-product_price=""
                    data-product_variant=""
                    data-product_brand=""
                    data-product_category=""
                    data-product_list=""
                    data-product_position=""
            >
                <a rel="nofollow" href="<?php echo $productURL ?>" class="product-box__img-field">
                    <div class="product-box__img-field__inner">
                        <img alt="" src="<?php echo $product['image'] ?>" data-src="" class="product-box__product-img">
                    </div>
                </a>
                <div class="product-box__meta-field">
                    <div class="product-box__meta-field__col-1">
                        <div class="product-box__meta-star">
                            <i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-gray-light"></i>
                        </div>
                        <div class="product-box__meta-comment">0 bình luận</div>
                    </div>
                    <div class="product-box__meta-field__col-2">
                        Từ &nbsp;<i class="svg -svg-16px align--bottom -svg-flag-circle-<?php echo $lang ?>"></i>
                    </div>
                </div>
                <div class="product-box__product-title">
                    <a href="<?php echo $productURL ?>"><?php echo $product['title'] ?></a>
                </div>
                <div class="product-box__price-field">
                    <div class="js-price product-box__curr-price">
                        <a class="js-product-panel__href" href="<?php echo $productURL ?>">Click để xem báo giá</a>
                    </div>
                </div>
                <div class="product-box__price-alert">
                    * Giá đã bao gồm phí vận chuyển nội địa
                </div>
            </div>
        </div>
    <?php
    endforeach;
?>

<?php
    $arrGroupAsinMerchant = generateGetLatestPriceMultipleProductKey($arrAsinMerchant, 5);
?>
