<section id="category-block" class="category-block">
    <div class="category-block__head">
        <div class="category-block__title-segment">
            <div class="category-block__block-title">
                <h1 class="category-block__block-title-main">Tìm kiếm "<span class="text--color-blue"><?php echo $strKeywords ?></span>"
                </h1>
                <span class="category-block__block-title-sub"> / Tìm thấy <?php echo number_format($arrFadoData['resultCount']['totalResults']) ?> sản phẩm tại Amazon <?php echo $countryTxt ?>. - Trang <?php echo $page ?></span>
            </div>
        </div>


        <div class="category-block__sort-segment">

        </div>
    </div>

    <div class="product-panel-wrap js-search-result-block__content--<?php echo $lang ?>">
        <?php
            $arrAsinMerchant = [];
            foreach ($arrFadoData['blockRight'] as $product):
                $arrMerchantInfo = $product['merchant'];
                $arrLinkInfo = $product['link'];
                
                $productURL = $arrLinkInfo['url'] . '&smid=' . $arrMerchantInfo['id'] . '&lang=' . $lang;
                
                $arrAsinMerchant[] = $product['asin'] . '-' . $arrMerchantInfo['id']; // Asin - MerchantID
                ?>
                <div class="product-panel-wrap__col">
                    <div
                            class="product-panel js-product-block is-loading-price"
                            data-country-code="us"
                            data-get-final-price=""
                            data-asin="<?php echo $product['asin'] ?>"
                            data-merchant-id="<?php echo $arrMerchantInfo['merchant'] ?>"
                            data-product-title="<?php echo $product['title'] ?>"
                            data-product_id="<?php echo $product['asin'] . '-' . $arrMerchantInfo['id'] ?>"
                            data-product_name="<?php echo $product['title'] ?>"
                            data-product_price=""
                            data-product_variant=""
                            data-product_brand=""
                            data-product_category=""
                            data-product_list=""
                            data-product_position=""
                    >
                        <a
                                class="product-panel__img-field js-product-panel__href"
                                rel="nofollow"
                                href="<?php echo $productURL ?>"
                        >
                            <div class="product-panel__img-field__inner">
                                <img
                                        class="lazyload product-panel__product-img"
                                        src="<?php echo MZ_PLUGIN_PATH ?>/assets/images/iconsloading-globe-gray.png"
                                        data-src="<?php echo $product['image'] ?>"
                                        alt=""
                                />
                            </div>
                        </a>

                        <div class="product-panel__meta-field">
                            <div class="product-panel__meta-field__col-1">
                                <div class="product-panel__meta-star">
                                    <i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i><i class="fas fa-star text--color-yellow"></i><i class="fas fa-star-half-alt text--color-yellow"></i>
                                </div>

                                <div class="product-panel__meta-comment">
                                    0 bình luận
                                </div>
                            </div>

                            <div class="product-panel__meta-field__col-2">
                                Từ &nbsp;<i class="svg -svg-16px -svg-flag-circle-<?php echo $lang; ?>"></i>
                            </div>
                        </div><!-- .product-panel__meta-field -->

                        <div class="product-panel__product-title">
                            <a class="js-product-panel__href" href="<?php echo $productURL ?>">
                                <?php echo $product['title'] ?>
                            </a>
                        </div>

                        <div class="product-panel__price-field">
                            <div class="js-price product-panel__curr-price">
                                <a class="js-product-panel__href" href="<?php echo $productURL ?>">Click để xem báo giá</a>
                            </div>

                            <div class="product-panel__old-price">

                            </div>
                        </div><!-- .product-panel__price-field -->

                        <div class="product-box__price-alert">
                            * Giá trọn gói về Việt Nam
                        </div>

                        <div class="product-panel__expand">
                            <ul class="product-panel__feature-list">
                                <li>Bảo vệ người mua, giảm thiểu rủi ro</li>
                                <li>Giao hàng tận nhà, không lo thủ tục</li>
                            </ul>

                            <div class="product-panel__merchant-field">
                                <i class="svg -svg-20px -svg-shop"></i>
                                <span class="text--color-light">Người bán:</span>
                                <?php echo $arrMerchantInfo['displayName'] ?>
                            </div>

                            <div class="row -row-5px">
                                <div class="col">
                                    <a
                                            class="my-btn -btn-sm -btn-pill -btn-block -btn-grd-bg js-product-panel__href"
                                            href="<?php echo $productURL ?>"
                                    >
                                        Xem chi tiết
                                    </a>
                                </div>
                            </div>
                        </div><!-- .product-panel__expand -->
                    </div><!-- ,product-panel -->
                </div>
            <?php endforeach; ?>
    </div><!-- .product-panel-wrap -->
    
    <?php
        $arrPaging = $arrFadoData['paging']['pages'];
    ?>
    <div class="pagination-nav">
        <ul class="pagination-nav__page-list">
            <?php
                $strPagingHTML = '';
                $strURL        = '';
                foreach ($arrPaging as $page) {
                    $type   = $page['type'];
                    $strURL = generateSourceURL($page['link']['url'], $lang, 1);
                    $strURL = str_replace($translatedKeywordEncode, $originKeywordEncode, $strURL);
                    switch ($type) {
                        case 'back':
                            $strPagingHTML .= '<li class="pagination-nav__page-item"><a class="pagination-nav__change-page-btn -arrow -prev" href="' . $strURL . '"><div class="pagination-nav__change-page-btn__inner"><i class="far fa-angle-left"></i></div></a></li>';
                            break;
                        case 'link':
                            $strPagingHTML .= '<li class="pagination-nav__page-item"><a class="pagination-nav__change-page-btn -number" href="' . $strURL . '">' . $page['link']['text'] . '</a></li>';
                            break;
                        case 'current':
                            $strPagingHTML .= '<li class="pagination-nav__page-item"><div class="pagination-nav__lbl-btn is-active">' . $page['link']['text'] . '</div></li>';
                            break;
                        case 'more':
                            $strPagingHTML .= '<li class="pagination-nav__page-item"><div class="pagination-nav__lbl-btn -dots">...</div></li>';
                            break;
                        case 'disabled':
                            $strPagingHTML .= '<li class="pagination-nav__page-item"><div class="pagination-nav__lbl-btn">' . $page['link']['text'] . '</div></li>';
                            break;
                        case 'next':
                            $strPagingHTML .= '<li class="pagination-nav__page-item"><a class="pagination-nav__change-page-btn -arrow -next" href="' . $strURL . '"><div class="pagination-nav__change-page-btn__inner"><i class="far fa-angle-right"></i></div></a></li>';
                            break;
                        default:
                            break;
                    }
                }
                echo $strPagingHTML;
            ?>
        </ul>
    </div>
</section>

<?php
    $arrGroupAsinMerchant = generateGetLatestPriceMultipleProductKey($arrAsinMerchant, 5);
?>